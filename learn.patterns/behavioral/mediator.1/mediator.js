/*****************************************************************************
 *
 *  File Name: mediator.js
 *
 *  Project: VMSClient
 *
 *  Author: ahristov
 *
 *  This material contains, and is a part of a computer software program
 *  which is, proprietary and confidential information owned by
 *  Verizon Corporate Technology.
 *  The program, including this material, may not be duplicated, disclosed
 *  or reproduced in whole or in part for any purpose without the express
 *  written authorization of Verizon Corporate Technology.  All authorized
 *  reproductions must be marked with this legend.
 *
 *  Co 2012 Verizon Corporate Technology, Inc.
 *  All rights reserved.
 *
 *****************************************************************************/

(function() {


	function Mediator() {

		function enableAll() {
			this.btnBook.disabled = false;
			this.btnView.disabled = false;
		}

		this.registerBook = function(btnBook) {
			this.btnBook = btnBook;
		};

		this.registerView = function(btnView) {
			this.btnView = btnView;
		};

		this.registerDisplay = function(divDisplay) {
			this.divDisplay = divDisplay;
		};

		this.book = function() {
			this.btnBook.disabled = true;
			this.btnView.disabled = true;
			this.divDisplay.innerHTML = "booking...";

			setTimeout(function() {
					this.divDisplay.innerHTML = "booked.";
					enableAll();
				},
				1000);
		}

		this.view = function() {
			this.divDisplay.innerHTML = "here is the last book";

			setTimeout(enableAll, 1000);
		}
	}


	function BtnView(btnView, mediator) {

		mediator.registerView(btnView);

		this.onclick = function() {
			mediator.view();
		}
	}

	function BtnBook(btnBook, mediator) {

		mediator.registerBook(btnBook);

		this.onclick = function() {
			mediator.book();
		}
	}

	function DivDisplay(divDisplay, mediator) {
		mediator.registerDisplay(divDisplay);
	}


	function extend(obj, iface) {
		for(var key in iface) {
			obj[key] = iface[key];
		}
	}


	var btnBook = document.getElementById("btnBook");
	var btnView = document.getElementById("btnView");
	var divDisplay = document.getElementById("divDisplay");

	var mediator = new Mediator();

	extend(btnBook, new BtnBook(btnBook, mediator));
	extend(btnView, new BtnView(btnView, mediator));
	extend(divDisplay, new DivDisplay(divDisplay, mediator));



}());



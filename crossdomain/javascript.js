/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 2/6/12
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */


var JSUtilities = (function () {

	/**
	 * Create XMLHttprRequest object.
	 * @return {XMLHttpRequest} XMLHttpRequest object.
	 */
	function getXMLHttpRequest() {

		var i, xhr, activeXids = [
			'Msxml2.XMLHTTP',
			'Msxml2.XMLHTTP.3.0',
			'Microsoft.XMLHTTP',
		];

		if (window.XMLHttpRequest) { // native XHR
			xhr = new XMLHttpRequest();
		} else if (window.ActiveXObject) { // IE before 7
			for (i=0; i < activeXids.length; i+=1) {
				try {
					xhr = new ActiveXObject(activeXids[i]);
					break;
				} catch(e) {}
			}
		}

		return xhr;
	}


	/**
	 * Returns JSON from the responseText
	 * @param {String} responseText the response text as it came from the XMLHttpRequest.
	 * @return {Object} JSON or empty object.
	 */
	function parseResponseToJSON(responseText) {

//        responseText = (responseText === "Not found") ? '' : responseText;
//        responseText = responseText || '{}';

		var result;

		try {
			result = JSON.parse(responseText);
		}
		catch (e) {
			console.error("Error parsing result: " + e.message);
			console.error("Response text:" + responseText);
			result = {ReturnValue: -1};
		}

		return result;
	}

	/**
	 * Post command to url.
	 *
	 * @param {String} command The command to post
	 * @param {String} url The URL to post to
	 * @returns {String} the responseText from the request.
	 */
	function postCommand(command, url) {
		var xmlHttp = getXMLHttpRequest();
		var responseText = '';

		if (xmlHttp) {

			xmlHttp.open("POST", url, false); // do not do async calls for now.
			xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

			try {
				xmlHttp.send(command);
				responseText = xmlHttp.responseText;
			}
			catch (e) {
				console.log("There has been an error to POST to " + url);
				console.log("Error message:" + e.message);
				responseText = '';
			}
		}

		return responseText;
	}

	/**
	 * not sure if good idea...
	 */
	function pause(milliseconds) {
		milliseconds += new Date().getTime();
		//while (new Date() < milliseconds) {}
	}


	return {

		// getXMLHttpRequest:getXMLHttpRequest
		postCommand:postCommand,
		parseResponseToJSON:parseResponseToJSON,
		pause:pause

	};

}());





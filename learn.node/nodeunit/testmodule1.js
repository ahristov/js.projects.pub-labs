/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 2/7/12
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */

// nodeunit --reporter junit  testmodule1.js --output junit.out
// cat junit.out\testmodule1.js.xml
//

module.exports = {
	setUp: function (callback) {
		this.foo = 'bar';
		callback();
	},
	tearDown: function (callback) {
		// clean up
		callback();
	},
	"test - If it is equal to foo": function (test) {
		test.equals(this.foo, 'bar');
		test.done();
	}
};


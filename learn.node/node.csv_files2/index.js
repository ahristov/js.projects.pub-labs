/**
 * Created with JetBrains PhpStorm.
 * User: ahristov
 * Date: 2/25/12
 * Time: 2:27 PM
 * To change this template use File | Settings | File Templates.
 */

var http = require('http');
var url = require('url');
var fs = require('fs');


var peopleDict = loadPeopleDict();

process.on('SIGINT',
	function() {
		console.log("Goodbye!");
		process.exit(0);
	});


function loadPeopleDict() {

	var peopleDict = {};

	fs.readdir('csv_data', function(err, files) {
		files.forEach(function(fileName) {
			fs.readFile('csv_data/' + fileName, 'utf-8', function(err, data) {
				console.log(data);
				var names = data.split(',');
				names.forEach(function (name) {

					name = name.trim();
					if (peopleDict[name] === undefined) {
						peopleDict[name] = 1;
					} else {
						peopleDict[name]++;
					}

				});
			});
		});
	});

	return peopleDict;
}


var server = http.createServer(function(req, res){

	var parsedUrl = url.parse(req.url, parseQueryString=true);

	if (parsedUrl.query.userName) {

		var userName= parsedUrl.query.userName.trim().toLowerCase();

		if (peopleDict[userName]) {
			res.end(userName + ' has attended ' + peopleDict[userName] + ' times.');
		} else {
			res.end(userName + ' never attended :(');
		}

		res.end(userName);

	} else {

		var page = ''
			+ '<!DOCTYPE html>'
			+ '<form>'
			+ '<input type="text" id="userName" name="userName" placeholder="enter name" required="required" />'
			+ '<button type="submit">Do it!</button>'
			+ '</form>';

		res.end(page);
	}
}).listen(8888);

console.log('Server is running at http://127.0.0.1:8888/');



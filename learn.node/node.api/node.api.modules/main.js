/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 3/4/12
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */

console.log('main starting');
var a = require('./a.js');
var b = require('./b.js');
console.log('in main, a.done=%j, b.done=%j', a.done, b.done);

require('./library-1');
require('./library-2');

require('library-3');
require('library-4');
console.log('library-4 is loaded from:' + require.resolve('library-4'));

var eventEmitter = require('./eventEmiter.js'); // require() returns the exports

eventEmitter.on('ready', function() {
	console.log('eventEmitter is ready');
});


/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 3/4/12
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */

var PI = Math.PI;

exports.area = function(r) {
	return PI * r * r;
};

exports.circumrefence = function(r) {
	return 2 * PI * r;
};

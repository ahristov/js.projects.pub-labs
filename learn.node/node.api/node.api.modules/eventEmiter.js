/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 3/4/12
 * Time: 12:37 PM
 * To change this template use File | Settings | File Templates.
 */


var EventEmitter = require('events').EventEmitter;

// assignent to module.exports can be done here,
// and not in a callback below
//
module.exports = new EventEmitter();

// Do some work, and after some time emit
// the 'ready' event from the module itself.
//

setTimeout(function() {
	// ... but we can call emit() to trigger events in a callback
	//
	module.exports.emit('ready');
}, 1000);


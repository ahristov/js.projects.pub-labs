describe("String", function() {

	var string;

	beforeEach(function() {
		string = "String";
	});

	afterEach(function() {
		delete string;
	});


	describe("When not empty", function() {

		it("Should have length greater than zero", function() {
			expect(string.length).toBeGreaterThan(0);
		});

	});


});



/**
 * Utility Query String: Parse the query string from window.location:
 *
 * - the parameter names are property names
 * - for single parameter the value will be string
 * - for multiple parameter the value will be an array of strings
 * - for parameter without value, the value will be 'undefined'
 *
 * @returns {Object} Contains the parameters as properties.
 *
 */
var QueryString = (function () {

	// This function is anonymous, is executed immediately and
	// the return value is assigned to QueryString!
	var query_string = {};

	// The following is not gonna work, because the "SVG Browser"
	// does not include the leading "?" in the window.location.search
	//
	// var query = window.location.search.substring(1);
	//
	// so, this is instead:
	//

	var query = window.location.search.replace(/^\?/, '');

	var vars = query.split("&");

	for (var i=0;i<vars.length;i++) {

		var pair = vars[i].split("=");

		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];

			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;

			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);

		}
	}

	return query_string;

}());

/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 4/2/12
 * Time: 1:41 PM
 * To change this template use File | Settings | File Templates.
 */

describe("Math", function() {

	var pi;

	beforeEach(function() {
		pi = Math.PI;
	});

	afterEach(function() {
		delete pi;
	});


	describe("PI", function() {

		it("Is somewhere between 3 and 4", function() {
			expect(pi).toBeGreaterThan(3);
			expect(pi).toBeLessThan(4);
		});

	});


});



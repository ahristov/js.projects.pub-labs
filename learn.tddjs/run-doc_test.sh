#! /usr/bin/env bash


docco code/test/usage_jstestdriver.js
docco code/test/usage_jshamcrest.js
docco code/test/usage_jsmockito.js
docco code/test/usage_underscore.js
docco code/test/jsbasics_test.js

rm -rf doc/test
mkdir -p doc/test

cp -rf docs/*.* doc/test/
rm -rf docs
rm -rf "\-p"


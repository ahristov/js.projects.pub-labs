
REM java -jar tools\JsTestDriver.jar --config code\jsTestDriver.conf --tests all 
REM --browser "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe,C:\Program Files (x86)\Opera\opera.exe" ^

cd code

java -jar ..\tools\JsTestDriver.jar ^
	--config jsTestDriver.conf ^
	--tests all ^
	--testOutput . ^
	--server http://localhost:42442

cd ..



call docco code\test\usage_jstestdriver.js
call docco code\test\usage_jshamcrest.js
call docco code\test\usage_jsmockito.js
call docco code\test\usage_underscore.js
call docco code\test\usage_backbone.js
call docco code\test\jsbasics_test.js

rmdir /S /Q doc\test
md doc
md doc\test

copy docs\*.* doc\test\*
rmdir /S /Q docs
rmdir /S /Q "-p"



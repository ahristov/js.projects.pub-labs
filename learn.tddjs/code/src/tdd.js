/**
 * Contains tools for supporting TDD and debugging.
 * @class Tdd
 * @author Atanas Hristov
 * @docauthor Atanas Hristov
 */
Tdd = (function() {
	
	/**
	 * If the expression **expr** is false, throws an exception.
	 * 
	 *		// Usage example:
	 *		Tdd.assert("error", false);
	 * 
	 * @param {String} message Error message for the exception.
	 * @param {Object} expr Expression to evaluate.
	 */
	function assert(message, expr) {
		if (!expr) {
			throw new Error(message);
		}
		
		assert.count += 1;
		
		return true;
	}
	assert.count = 0;

	/**
	 * Gets count of passed asserts.
	 */
	function assertCountPassed() {
		return assert.count;
	}
	
	/**
	 * Reset count of passed asserts to zero. 
	 */
	function assertResetCountPassed() {
		assert.count = 0;
	}
	
	
	var benchmark = (function() {
		
		function init(name) {
			var container = document.createElement("div");
			container.id = "tdd.benchmark";
			
			var heading = document.createElement("h2");
			heading.innerHTML = name;
			container.appendChild(heading);
			
			var ol = document.createElement("ol");
			container.appendChild(ol);
			
			document.body.appendChild(container);
			
			return ol;
		}
		
		function runTests(tests, view, iterations) {
			
			for (var label in tests) {
				if (!tests.hasOwnProperty(label) ||
						typeof tests[label] != "function") {
					continue;
				}
			
				(function(name, test) {
					//setTimeout(function() {
						var start = new Date().getTime();
						var l = iterations;
						
						if (!test.length) {
							while (l--) {
								test();
							}
						} else {
							test(l);
						}
						
						var total = new Date().getTime() - start;
						var li = document.createElement("li");
						var average = total / iterations;
						
						li.innerHTML = name + ": " + total + 
							"ms (total), " + average + 
							"ms (avg)";
						view.appendChild(li);
						
						tests[label].total = total;
						tests[label].average = average;
						
					//}, 15);
				}(label, tests[label]));
			}
		}
		
		var result = function (name, tests, iterations) {
			iterations = iterations || 1000;
			var view = init(name);
			runTests(tests, view, iterations);
		};
		
		return result;
		
	}());
	
	
	
	return {
		assert : assert,
		assertCountPassed : assertCountPassed,
		assertResetCountPassed : assertResetCountPassed,
		benchmark : benchmark
	};
}());




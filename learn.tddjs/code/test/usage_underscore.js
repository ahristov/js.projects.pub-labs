// [Underscore](http://documentcloud.github.com/underscore/) is a utility-belt library 
// for JavaScript that provides a lot of the functional programming support.
//
// Underscore provides 60-odd functions that support both the usual functional suspects: 
// map, select, invoke - as well as more specialized helpers: function binding, 
// javascript templating, deep equality testing, and so on. 
// 
// It delegates to built-in functions, if present, so modern browsers will use the native 
// implementations of forEach, map, reduce, filter, every, some and indexOf.
//

TestCase("Usage of Underscore", {

	
	setUp : function() {

		/*:DOC +=

<div class="page">
	<div id="msg">There has been an error on this page</div>
	<form id="frm" action="get">
		<input type="button" id="btnSubmit" value="Submit"></input>
	</form>
</div>

		*/

	},


	"test - Usage of Underscore" : function() {

		// - - -
		//
		// ### Collection Functions (Arrays or Objects)
		//
		// - - -

		
		// - - -
		//
		// **each** `_.each(list, iterator, [context])` Alias: *forEach*:
		//
		// Iterates over a list of elements, yielding each in turn to an iterator function. 
		// The iterator is bound to the context object, if one is passed.  
		//
		// Each invocation of iterator is called with three arguments: `(element, index, list)`.
		//
		// If list is a JavaScript object, iterator's arguments will be `(value, key, list)`.
		//
		// Delegates to the native *forEach* function if it exists.
		//
		
		_.each([1, 2, 3],
			function(element) {
				assertThat(element,  between(1).and(3));
			});

		_.each({one: 11, two: 12, three: 13}, 
			function(value, key) {
				assertThat(value, between(11).and(13));
				assertThat(key, isIn(['one', 'two', 'three']));
				jstestdriver.console.log("_each: value="+value+" key="+key+"");
			});

		
		// - - -
		//
		// **map** `_.map(list, iterator, [context])` Alias: *collect* 
		//
		// Produces a new array of values by mapping each value in list 
		// through a transformation function (iterator).  
		//
		// If the native map method exists, it will be used instead.
		//
		// If list is a JavaScript object, iterator's arguments 
		// will be `(value, key, list)`.		
		//
		
		var list = [1, 2, 3];
		var mult = _.map(list, function(element) 
			{return element * 3;}); // => [3, 6, 9]
		assertThat(mult, allOf(hasSize(3), hasItems(3, 6, 9)));

		var obj = {one : 1, two : 2, three : 3}; 
		var mult = _.map(obj, function(value, key)
			{ return value * 3; }); // => [3, 6, 9]
		assertThat(mult, allOf(hasSize(3), hasItems(3, 6, 9)));

		
		// - - -
		//
		// **reduce** `_.reduce(list, iterator, memo, [context])` Aliases: *inject, foldl*
		//
		// Also known as inject and foldl, reduce boils down a list of values 
		// into a single value.   
		//
		// Memo is the initial state of the reduction, and each successive 
		// step of it should be returned by iterator.		
		//
		
		var list = [1, 2, 3];
		var sum = _.reduce(list, function(memo, num) { return memo + num; }, 0);
		assertThat(sum, equalTo(6));

		var arr = ['a','b','a','b','c'];
		var result = _.reduce(arr,
			function(memo, letter) {
				memo[letter] = (memo[letter] || 0) + 1;
				return memo
			}, {}); // { a: 2, b: 2, c: 1 }

		assertTrue(_.has(result, "a"));
		assertThat(result.a, equalTo(2));

		assertTrue(_.has(result, "b"));
		assertThat(result.b, equalTo(2));

		assertTrue(_.has(result, "c"));
		assertThat(result.c, equalTo(1));

		// - - -
		//
		// **reduceRight** `_.reduceRight(list, iterator, memo, [context])` Alias: *foldr* 
		//
		// The right-associative version of reduce. Delegates to the JavaScript 1.8 version 
		// of reduceRight, if it exists. 
		//
		// Foldr is not as useful in JavaScript as it would be in a language with lazy evaluation.		
		//
		
		var list = [[0, 1], [2, 3], [4, 5]];
		var flat = _.reduceRight(list, function(a, b) 
			{ return a.concat(b); }, []); // => [4, 5, 2, 3, 0, 1]
		assertThat(flat, allOf(hasSize(6), hasItems(4, 5, 2, 3, 0, 1)));
		

		// - - -
		//
		// **find** `_.find(list, iterator, [context])` Alias: *detect* 
		//
		// Looks through each value in the list, returning the first one 
		// that passes a truth test (iterator). 
		//
		// The function returns as soon as it finds an acceptable element, 
		// and doesn't traverse the entire list.
		//
		
		var list = [1, 2, 3, 4, 5, 6];
		var even = _.find(list, function(num){ return num % 2 == 0; });
		assertThat(even, equalTo(2));


		// - - -
		//
		// **filter** `_.filter(list, iterator, [context])` Alias: *select* 
		//
		// Looks through each value in the list, returning an array 
		// of all the values that pass a truth test (iterator). 
		//
		// Delegates to the native filter method, if it exists.		
		//
		
		var list = [1, 2, 3, 4, 5, 6];
		var evens = _.filter(list, function(num) 
			{ return num % 2 == 0; }); // => [2, 4, 6]
		assertThat(evens, allOf(hasSize(3), hasItems(2,4,6)));

		
		// - - -
		//
		// **reject** `_.reject(list, iterator, [context])`
		//
		// Returns the values in list without the elements that 
		// the truth test (iterator) passes. *The opposite of filter*.
		//
		
		var list = [1, 2, 3, 4, 5, 6];
		var odds = _.reject(list, function(num) 
			{ return num % 2 == 0; }); // => [1, 3, 5]
		assertThat(odds, allOf(hasSize(3), hasItems(1,3,5)));

		
		// - - -
		//
		// **all** `_.all(list, iterator, [context])` Alias: *every* 
		//
		// Returns true if all of the values in the list pass 
		// the iterator truth test. 
		//
		// Delegates to the native method every, if present.		
		//
		
		var chk = _.all([true, 1, null, 'yes'], _.identity);
		assertThat(chk, equalTo(false));

		
		// - - -
		//
		// **any** `_.any(list, [iterator], [context])` Alias: *some* 
		// 
		// Returns true if any of the values in the list pass 
		// the iterator truth test. Short-circuits and stops traversing 
		// the list if a true element is found. 
		//
		// Delegates to the native method some, if present.
		//
		
		var chk = _.any([null, 0, 'yes', false]);
		assertThat(chk, equalTo(true));

		
		// - - -
		//
		// **include** `_.include(list, value)` Alias: *contains* 
		//
		// Returns true if the value is present in the list, 
		// using === to test equality. 
		//
		// Uses indexOf internally, if list is an Array.
		//
		
		var chk = _.include([1, 2, 3], 3);
		assertThat(chk, equalTo(true));

		
		// - - -
		//
		// **invoke** `_.invoke(list, methodName, [*arguments])` 
		//
		// Calls the method named by methodName on each value in the list. 
		//
		// Any extra arguments passed to invoke will be forwarded on 
		// to the method invocation.
		//
		
		var list = [[5, 1, 7], [3, 2, 1]];
		var sorted = _.invoke(list, 'sort'); // => [[1, 5, 7], [1, 2, 3]]
		assertThat(sorted, hasSize(2));
		assertThat(sorted[0], hasSize(3));
		assertThat(sorted[0][0], equalTo(1));
		assertThat(sorted[0][1], equalTo(5));
		assertThat(sorted[0][2], equalTo(7));


		// - - -
		//
		// **pluck** `_.pluck(list, propertyName)`
		//
		// A convenient version of what is perhaps 
		// the most common use-case for map: 
		// extracting a list of property values.
		//
		
		var stooges = [ {name : 'moe', age : 40}, 
		                {name : 'larry', age : 50}, 
		                {name : 'curly', age : 60} ];
		var names = _.pluck(stooges, 'name'); // => ["moe", "larry", "curly"]
		assertThat(names, allOf(hasSize(3), hasItems("moe", "larry", "curly")));

		
		// - - -
		//
		// **max** `_.max(list, [iterator], [context])` 
		//
		// Returns the maximum value in list. 
		//
		// If iterator is passed, it will be used on each value 
		// to generate the criterion by which the value is ranked.
		//
		
		var stooges = [ {name : 'moe', age : 40}, 
		                {name : 'larry', age : 50}, 
		                {name : 'curly', age : 60} ];
		var maxStg = _.max(stooges, function(stooge) 
			{ return stooge.age; }); // => {name : 'curly', age : 60};
		assertThat(maxStg.name, equalTo('curly'));
		assertThat(maxStg.age, equalTo(60));

		
		// - - -
		//
		// **min** `_.min(list, [iterator], [context])` 
		//
		// Returns the minimum value in list. 
		//
		// If iterator is passed, it will be used on each value 
		// to generate the criterion by which the value is ranked.
		//
		
		var list = [10, 5, 100, 2, 1000];
		var minVal = _.min(list); // => 2
		assertThat(minVal, equalTo(2));

		
		// - - -
		//
		// **sortBy** `_.sortBy(list, iterator, [context])` 
		//
		// Returns a sorted copy of list, ranked in ascending order 
		// by the results of running each value through iterator.
		//
		
		var list = [1, 2, 3, 4, 5, 6];
		var sorted = _.sortBy(list, function(num)
			{ return Math.sin(num); }); // => [5, 4, 6, 3, 1, 2]
		assertThat(sorted[0], equalTo(5));
		assertThat(sorted[1], equalTo(4));
		assertThat(sorted[2], equalTo(6));
		assertThat(sorted[3], equalTo(3));
		assertThat(sorted[4], equalTo(1));
		assertThat(sorted[5], equalTo(2));

		// - - -
		//
		// **groupBy** `_.groupBy(list, iterator)` 
		//
		// Splits a collection into sets, grouped by the result 
		// of running each value through iterator. 
		//
		// If iterator is a string instead of a function, 
		// groups by the property named by iterator on each of the values.
		//
		
		var list = [1.3, 2.1, 2.4];
		var groped = _.groupBy(list, function(num) 
			{ return Math.floor(num); }); // => {1: [1.3], 2: [2.1, 2.4]}
		assertThat(groped[1], hasSize(1));
		assertThat(groped[1], hasItems(1.3));
		assertThat(groped[2], hasSize(2));
		assertThat(groped[2], hasItems(2.1, 2.4));

		var list = ['one', 'two', 'three'];
		var grouped = _.groupBy(list, 'length'); // => {3: ["one", "two"], 5: ["three"]}
		assertThat(grouped[3], hasSize(2));
		assertThat(grouped[3], hasItems("one", "two"));
		assertThat(grouped[5], hasSize(1));
		assertThat(grouped[5], hasItems("three"));
		
		// - - -
		//
		// **sortedIndex** `_.sortedIndex(list, value, [iterator])` 
		// 
		// Uses a binary search to determine the index at which the value 
		// should be inserted into the list in order to maintain 
		// the list's sorted order.
		//
		// If an iterator is passed, it will be used 
		// to compute the sort ranking of each value.
		//
		
		var list = [10, 20, 30, 40, 50];
		var idx = _.sortedIndex(list, 35); // => 3
		assertThat(idx, equalTo(3));
		
		// - - -
		//
		// **shuffle** `_.shuffle(list)`
		// 
		// Returns a shuffled copy of the list, 
		// using a version of the Fisher-Yates shuffle.
		//
		
		var list = [1, 2, 3, 4, 5, 6];
		var shuffled = _.shuffle(list); // => [4, 1, 6, 3, 5, 2]
		assertThat(shuffled, hasSize(6));
		
		// - - -
		//
		// **toArray** `_.toArray(list)	 
		// 
		// Converts the list (anything that can be iterated over), 
		// into a real Array. Useful for transmuting the arguments object.
		//
		
		var list = (function(){ 
				return _.toArray(arguments).slice(0); 
			})(1, 2, 3);
		assertThat(list, hasSize(3));
		assertThat(list[0], equalTo(1));
		assertThat(list[1], equalTo(2));
		assertThat(list[2], equalTo(3));
		
		// - - -
		//
		// **size** `_.size(list)` 
		//
		// Return the number of values in the list.
		//
		
		var obj = {one : 1, two : 2, three : 3};
		var sz  = _.size(obj); // => 3
		assertThat(sz, equalTo(3));






		// - - -
		//
		// ### Array Functions
		//
		// *Note: All array functions will also work on the **arguments** object.*
		//
		// - - -

		
		// - - -
		//
		// **first** `_.first(array, [n])` Alias: *head* 
		//
		// Returns the first element of an array. 
		//
		// Passing n will return the first n elements of the array.		
		//
		
		var list = [5, 4, 3, 2, 1];
		var el = _.first(list); // => 5
		assertThat(el, equalTo(5));

		// - - -
		//
		// **initial** `_.initial(array, [n])`
		//
		// Returns everything but the last entry of the array. 
		//
		// Especially useful on the arguments object. 
		//
		// Pass n to exclude the last n elements from the result.
		//
		
		var list = [5, 4, 3, 2, 1];
		var result = _.initial(list); // => [5, 4, 3, 2]
		assertThat(result, allOf(hasSize(4), hasItems(5, 4, 3, 2)));

		

		// - - -
		//
		// **last** `_.last(array, [n])`
		//
		// Returns the last element of an array. 
		//
		// Passing n will return the last n elements of the array.
		//
		
		var list = [5, 4, 3, 2, 1];
		var result = _.last(list); // => 1
		assertThat(result, equalTo(1));


		// - - -
		//
		// **rest** `_.rest(array, [index])` Alias: *tail* 
		//
		// Returns the rest of the elements in an array. 
		//
		// Pass an index to return the values of the array 
		// from that index onward.
		//
		
		var list = [5, 4, 3, 2, 1];
		var result = _.rest(list); // => [4, 3, 2, 1]
		assertThat(result, allOf(hasSize(4), hasItems(4,3,2,1)));

		
		// - - -
		//
		// **compact** `_.compact(array)`
		//
		// Returns a copy of the array with all falsy values removed. 
		//
		// In JavaScript, false, null, 0, "", undefined and NaN are all falsy.
		//
		
		var list = [0, 1, false, 2, '', 3];
		var result = _.compact(list); // => [1, 2, 3]
		assertThat(result, allOf(hasSize(3), hasItems(1, 2, 3)));



		// - - -
		//
		// **flatten** `_.flatten(array, [shallow])`
		//
		// Flattens a nested array (the nesting can be to any depth). 
		//
		// If you pass shallow, the array will only be flattened a single level.
		//
		
		var list = [1, [2], [3, [[4]]]];
		var result = _.flatten(list); // => [1, 2, 3, 4];
		assertThat(result, allOf(hasSize(4), hasItems(1, 2, 3, 4)));
		
		
		var list = [1, [2], [3, [[4]]]];
		var result = _.flatten(list, true); // => [1, 2, 3, [[4]]];
		assertThat(result, allOf(hasSize(4), hasItems(1, 2, 3)));
		
		
		// - - -
		//
		// **without** `_.without(array, [*values])`
		//
		// Returns a copy of the array with all instances of the values removed. 
		//
		// === is used for the equality test.
		//
		
		var list = [1, 2, 1, 0, 3, 1, 4];
		var result = _.without(list, 0, 1); // => [2, 3, 4]
		assertThat(result, allOf(hasSize(3), hasItems(2, 3, 4), not(hasItems(0, 1))));
		
		
		// - - -
		//
		// **union** `_.union(*arrays)`
		//
		// Computes the union of the passed-in arrays: 
		// the list of unique items, in order, that are present 
		// in one or more of the arrays.
		//
		
		var result = _.union(
			[1, 2, 3], 
			[101, 2, 1, 10], 
			[2, 1]); // => [1, 2, 3, 101, 10]
		assertThat(result, anyOf(hasSize(5), hasItems(1,2,3,101,10)));
		
		
		
		// - - -
		//
		// **intersection** `_.intersection(*arrays)`
		//
		// Computes the list of values that are the intersection 
		// of all the arrays. 
		//
		// Each value in the result is present in each of the arrays.
		//
		
		var result = _.intersection(
			[1, 2, 3], 
			[101, 2, 1, 10], 
			[2, 1]); // => [1, 2]
		assertThat(result, anyOf(hasSize(2), hasItems(1,2)));
		
		// - - -
		//
		// **difference** `_.difference(array, *others)`
		//
		// Similar to without, but returns the values from array 
		// that are not present in the other arrays.
		//
		
		var result = _.difference(
			[1, 2, 3, 4, 5], 
			[5, 2, 10]); // => [1, 3, 4]
		assertThat(result, anyOf(hasSize(3), hasItems(1,3,4)));
		
		
		// - - -
		//
		// **uniq** `_.uniq(array, [isSorted], [iterator])` Alias: *unique*
		//
		// Produces a duplicate-free version of the array, 
		// using === to test object equality.  
		//
		// If you know in advance that the array is sorted, 
		// passing true for isSorted will run a much faster algorithm.  
		//
		// If you want to compute unique items based on a transformation, 
		// pass an iterator function.
		//
		
		var result = _.uniq([1, 2, 1, 3, 1, 4]); // => [1, 2, 3, 4]
		assertThat(result, anyOf(hasSize(4), hasItems(1,2,3,4)));
		    

    
		// - - -
		//
		// **zip** `_.zip(*arrays)`
		//
		// Merges together the values of each of the arrays 
		// with the values at the corresponding position.   
		//
		// Useful when you have separate data sources that are 
		// coordinated through matching array indexes.  
		//
		// If you're working with a matrix of nested arrays, 
		// zip.apply can transpose the matrix in a similar fashion.
		//
		
		var result = _.zip(
			['moe', 'larry', 'curly'], 
			[30, 40, 50], 
			[true, false, false]); // => [["moe", 30, true], ["larry", 40, false], ["curly", 50, false]]
		assertThat(result, hasSize(3));
		assertThat(result[0], anyOf(hasSize(3), hasItems("moe", 30, true)));
		
		// - - -
		//
		// **indexOf** `_.indexOf(array, value, [isSorted])`
		//
		// Returns the index at which value can be found in the array, 
		// or -1 if value is not present in the array.   
		//
		// Uses the native indexOf function unless it's missing.  
		//
		// If you're working with a large array, and you know that 
		// the array is already sorted, pass true for isSorted 
		// to use a faster binary search.
		//
		
		var result = _.indexOf([1, 2, 3], 2); // => 1
		assertThat(result, equalTo(1));
		

		// - - -
		//
		// **lastIndexOf** `_.lastIndexOf(array, value)`
		//
		// Returns the index of the last occurrence of value in the array, 
		// or -1 if value is not present. 
		//
		// Uses the native lastIndexOf function if possible.
		//
		
		var result = _.lastIndexOf([1, 2, 3, 1, 2, 3], 2); // => 4		
		assertThat(result, equalTo(4));
		

		// - - -
		//
		// **range** `_.range([start], stop, [step])`
		//
		// A function to create flexibly-numbered lists of integers, 
		// handy for each and map loops. 
		//
		// start, if omitted, defaults to 0;
		//
		// step defaults to 1. 
		//
		// Returns a list of integers from start to stop, 
		// incremented (or decremented) by step, exclusive.
		//
		
		var result = _.range(10); // => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
		assertThat(result, allOf(hasSize(10), everyItem(between(0).and(9))));
		
		var result = _.range(1, 11); // => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
		assertThat(result, allOf(hasSize(10), everyItem(between(0).and(10))));
		
		var result = _.range(0, 30, 5); // => [0, 5, 10, 15, 20, 25]
		assertThat(result, allOf(hasSize(6), everyItem(between(0).and(25))));
		
		var result = _.range(0, -10, -1); // => [0, -1, -2, -3, -4, -5, -6, -7, -8, -9]
		assertThat(result, anyOf(hasSize(3), everyItem(between(-9).and(0))));
		
		var result = _.range(0); // => []
		assertThat(result, hasSize(0));
		    



		// - - -
		//
		// ### Function (uh, ahem) Functions
		//
		// - - -




		// - - -
		//
		// **bind** `_.bind(function, object, [*arguments])`
		//
		// Bind a function to an object, meaning that whenever 
		// the function is called, the value of this will be the object. 
		//
		// Optionally, bind arguments to the function to pre-fill them, 
		// also known as partial application.
		//
		
		var func = function(greeting) {
			return greeting + ': ' + this.name 
		};
		
		func = _.bind(func, {name : 'moe'}, 'hi');
		
		var result = func(); // => 'hi: moe'
		
		assertThat(result, equalTo("hi: moe"));




		// - - -
		//
		// **bindAll** `_.bindAll(object, [*methodNames])`
		//
		// Binds a number of methods on the object, specified by methodNames, 
		// to be run in the context of that object whenever they are invoked. 
		//
		// Very handy for binding functions that are going to be used as 
		// event handlers, which would otherwise be invoked with a fairly useless this. 
		//
		// If no methodNames are provided, all of the object's function properties 
		// will be bound to it.
		//

		var result = '';
		var btnSubmitData = {
		  label   : 'underscore',
		  onClick : function() {
				result = 'clicked: ' + this.label; 
			},
		  onHover : function() { 
				console.log('hovering: ' + this.label); 
			}
		};
		
		_.bindAll(btnSubmitData);
		
		var btnSubmit = document.getElementById('btnSubmit');
		
		btnSubmit.onclick = 
			btnSubmitData.onClick; // => When the button is clicked, this.label will have the correct value...
		btnSubmit.click();
		
		assertThat(result, equalTo('clicked: underscore'));
		

		// - - -
		//
		// **memoize** `_.memoize(function, [hashFunction])`
		//
		// Memoizes a given function by caching the computed result. 
		//
		// Useful for speeding up slow-running computations. 
		//
		// If passed an optional hashFunction, it will be used to compute 
		// the hash key for storing the result, based on the arguments 
		// to the original function. 
		// 
		// The default hashFunction just uses the first argument to the memoized function as the key.
		//

		function fibonacci(n) {
			return n < 2 ? n : fibonacci(n - 1) + fibonacci(n - 2);
		}
		
		var memoized = _.memoize(fibonacci);
		
		Tdd.benchmark("Memoized performance", {
			"fibonacci-plain"    : function() { fibonacci(20); },
			"fibonacci-memoized" : function() { memoized(20);  }
		},5);
		


		// - - -
		//
		// **delay** `_.delay(function, wait, [*arguments])` 
		//
		// Much like `setTimeout`, invokes function after wait milliseconds. 
		// 
		// If you pass the optional arguments, they will be forwarded on 
		// to the function when it is invoked.
		//

		var log = _.bind(console.log, console);
		_.delay(log, 1000, 'logged later'); // => 'logged later' // Appears after one second.

		

		// - - - 
		//
		// **defer** `_.defer(function)`
		//
		// Defers invoking the function until the current call stack has cleared, 
		// similar to using setTimeout with a delay of 0.
		//
		// Useful for performing expensive computations or HTML rendering in chunks 
		// without blocking the UI thread from updating.
		//
		
		_.defer(function(){ console.log('deferred'); }); // Returns from the function before console.log()
		

		// - - - 
		//
		// **throttle** `_.throttle(function, wait)`
		//
		// Creates and returns a new, throttled version of the passed function, 
		// that, when invoked repeatedly, will only actually call the original function 
		// at most once per every wait milliseconds. 
		//
		// Useful for rate-limiting events that occur faster than you can keep up with.
		//

		// var throttled = _.throttle(updatePosition, 100);
		// $(window).scroll(throttled);

		

		// - - -
		//
		// **debounce** `_.debounce(function, wait)`
		//
		// Creates and returns a new debounced version of the passed function 
		// that will postpone its execution until after wait milliseconds 
		// have elapsed since the last time it was invoked. 
		// 
		// Useful for implementing 
		// behavior that should only happen after the input has stopped arriving. 
		//
		// For example: rendering a preview of a Markdown comment, recalculating a 
		// layout after the window has stopped being resized, and so on.
		//

		// var lazyLayout = _.debounce(calculateLayout, 300);
		// $(window).resize(lazyLayout);



		// - - -
		//
		// **once** `_.once(function)`
		//
		// Creates a version of the function that can only be called one time. 
		//
		// Repeated calls to the modified function will have no effect, 
		// returning the value from the original call. 
		//
		// Useful for initialization functions, instead of having to 
		// set a boolean flag and then check it later.
		//
		
	
		var result = 0;
		function createApplication() { result++; }
		var initialize = _.once(createApplication);
		initialize();
		initialize(); // Application is only created once.		

		assertThat(result, equalTo(1));





		// - - -
		//
		// **after** `_.after(count, function)`
		//
		// Creates a version of the function that will only be run 
		// after first being called count times.
		//
		// Useful for grouping asynchronous responses, 
		// where you want to be sure that all the async calls have finished, 
		// before proceeding.
		//

		var result = 0;
		var count = 0;
		
		function runAfter(i) {
			count++;
			result = i;
		}
		
		var runAfterRef = _.after(3, runAfter);
		for (var i=0; i<3; i++) {
			runAfterRef(i);
		}
		
		assertThat(result, equalTo(2));
		assertThat(count, equalTo(1));
		





		// - - -
		//
		// **wrap** `_.wrap(function, wrapper)` 
		//
		// Wraps the first function inside of the wrapper function, 
		// passing it as the first argument. 
		//
		// This allows the wrapper to execute code before and after 
		// the function runs, adjust the arguments, and execute it conditionally.
		//

		var hello = function(name) { return "hello: " + name; };
		var result = "";
		hello = _.wrap(hello, function(func) {
			result = "before, " + func("moe") + ", after";
		});
		hello(); // => 'before, hello: moe, after'

		assertThat(result, equalTo("before, hello: moe, after"));
		



		// - - -
		//
		// **compose** `_.compose(*functions)`
		//
		// Returns the composition of a list of functions, 
		// where each function consumes the return value of 
		// the function that follows. 
		//
		// In math terms, composing the functions `f()`, `g()`, and `h()` 
		// produces `f(g(h()))`.
		//

		var greet    = function(name){ return "hi: " + name; };
		var exclaim  = function(statement){ return statement + "!"; };
		var welcome  = _.compose(exclaim, greet);
		var result   = welcome('moe'); // => 'hi: moe!'

		assertThat(result, equalTo('hi: moe!'));







		// - - -
		//
		// ### Object Functions
		// 
		// - - -

		
		// - - -
		//
		// **keys** `_.keys(object)` 
		//
		// Retrieve all the names of the object's properties.
		//
		
		var result = _.keys({
			one : 1, 
			two : 2, 
			three : 3
		}); // => ["one", "two", "three"]		

		assertThat(result, hasSize(3));
		assertThat(result[0], equalTo('one'));
		assertThat(result[1], equalTo('two'));
		assertThat(result[2], equalTo('three'));





		// - - -
		//
		// **values** `_.values(object)`
		//
		// Return all of the values of the object's properties.
		//
		
		var result = _.values({
			one : 1, 
			two : 2, 
			three : 3
		}); // => [1, 2, 3]
    
		assertThat(result, hasSize(3));
		assertThat(result[0], equalTo(1));
		assertThat(result[1], equalTo(2));
		assertThat(result[2], equalTo(3));




		// - - -
		//
		// **functions** `_.functions(object)` Alias: **methods**
		//
		// Returns a sorted list of the names of every method 
		// in an object - that is to say, the name of every
		// function property of the object.
		//
		
		
		var result = _.functions(_); // => ["all", "any", "bind", "bindAll", "clone", "compact", "compose" ...
		
		assertThat(result, hasItems("all", "any", "bind"));
		
		



		// - - -
		//
		// **extend** `_.extend(destination, *sources)`
		//
		// Copy all of the properties in the source objects over 
		// to the destination object. 
		//
		// It's in-order, so the last source will override properties 
		// of the same name in previous arguments.
		//
		
		
		var result = _.extend({name : 'moe'}, {age : 50}); // => {name : 'moe', age : 50}
		
		assertThat(result['name'] , equalTo('moe'));
		assertThat(result['age'] , equalTo(50));
    


		// - - -
		//
		// **defaults** `_.defaults(object, *defaults)`
		//
		// Fill in missing properties in object with default values 
		// from the defaults objects.
		//
		// As soon as the property is filled, further defaults will have no effect.
		//
		
		var iceCream = {flavor : "chocolate"};
		var result = _.defaults(iceCream, {
			flavor : "vanilla", 
			sprinkles : "lots"
		}); // => {flavor : "chocolate", sprinkles : "lots"}

		assertTrue(_.has(iceCream, 'flavor'));
		assertTrue(_.has(iceCream, 'sprinkles'));

		assertTrue(iceCream.hasOwnProperty('flavor'));
		assertTrue(iceCream.hasOwnProperty('sprinkles'));
		
		assertThat(iceCream.flavor, equalTo('chocolate'));
		assertThat(iceCream.sprinkles, equalTo('lots'));


		// - - -
		//
		// **clone** `_.clone(object)`
		//
		// Create a shallow-copied clone of the object. 
		//
		// Any nested objects or arrays will be copied by reference, not duplicated.
		//
		
		var result = _.clone({name : 'moe'}); // => {name : 'moe'};
		
		assertThat(result.name, equalTo('moe'));


		// - - -
		//
		// **tap** `_.tap(object, interceptor)`
		//
		// Invokes interceptor with the object, and then returns object. 
		//
		// The primary purpose of this method is to "tap into" a method chain, 
		// in order to perform operations on intermediate results within the chain.
		//
		// **Did not work!**
		//
		
		var result = _.chain([1,2,3,200])
			.filter(function(num) { return num % 2 == 0; })
			// .tap(console.log)
			.map(function(num) { return num * num })
			.value();
		

		// - - -
		//
		// **has** `_.has(object, key)`
		//
		// Does the object contain the given key? 
		// Identical to object.hasOwnProperty(key), but uses a safe reference 
		// to the hasOwnProperty function, in case it's been overridden accidentally.
		//
		
		var result = _.has({a: 1, b: 2, c: 3}, "b"); // => true
		assertTrue(result);





		// - - -
		//
		// **isEqual** `_.isEqual(object, other)`
		//
		// Performs an optimized deep comparison between the two objects, 
		// to determine if they should be considered equal.
		//
		
		var moe   = {name : 'moe', luckyNumbers : [13, 27, 34]};
		var clone = {name : 'moe', luckyNumbers : [13, 27, 34]};
		
		var result = moe == clone; // => false
		assertFalse(result);
		
		
		var result = _.isEqual(moe, clone); // => true
		assertTrue(result);





		// - - -
		//
		// **isEmpty** `_.isEmpty(object)`
		//
		// Returns true if object contains no values.
		//
		
		var result = _.isEmpty([1, 2, 3]); // => false
		assertFalse(result);
		
		var result = _.isEmpty({}); // => true
		assertTrue(result);




		// - - -
		//
		// **isElement** `_.isElement(object)`
		//
		// Returns true if object is a DOM element.
		//
		
		var result = _.isElement($('#msg')[0]); // => true

		assertTrue(result);
		
		assertTrue(_.isElement($('#msg')[0]));
		assertFalse(_.isElement($('#msg')));



		// - - -
		//
		// **isArray** `_.isArray(object)`
		//
		// Returns true if object is an Array.
		//
		
		var result = (function(){ 
			return _.isArray(arguments); 
		})(); // => false
		
		assertFalse(result);



		var result = (function(){ 
			return _.isArray(arguments); 
		})(1, 2, 3); // => false

		assertFalse(result);



		var result =  _.isArray([1,2,3]); // => true
		
		assertTrue(result);
		


		// - - -
		//
		// **isArguments** `_.isArguments(object)`
		//
		// Returns true if object is an Arguments object.
		//
		
		var result = (function(){ 
			return _.isArguments(arguments); 
		})(1, 2, 3); // => true
		
		assertTrue(result);
		
		
		var result = _.isArguments([1,2,3]); // => false
		
		assertFalse(result);
		
		

		// - - - 
		//
		// **isFunction** `_.isFunction(object)`
		//
		// Returns true if object is a Function.
		//
		
		var result = _.isFunction(alert); // => true
		
		assertTrue(result);
		


		// - - -
		//
		// **isString** `_.isString(object)`
		//
		// Returns true if object is a String.
		//
		
		var result = _.isString("moe"); // => true
		
		assertTrue(result);
		
		
		var result = _.isString(1);
		
		assertFalse(result);

		


		// - - -
		//
		// **isNumber** `_.isNumber(object)`
		//
		// Returns true if object is a Number (including NaN).
		//

		var result = _.isNumber(8.4 * 5); // => true

		assertTrue(result);



		var result = _.isNumber('1'); // false
		
		assertFalse(result);
		
		



		// - - -
		//
		// **isBoolean** `_.isBoolean(object)`
		//
		// Returns true if object is either true or false.

		var result = _.isBoolean(null); // => false
		
		assertFalse(result);


		var result = _.isBoolean(1===1); // true
		
		assertTrue(result);
		
		


		// - - -
		//
		// **isDate** `_.isDate(object)`
		//
		// Returns true if object is a Date.
		//
		
		var result = _.isDate(new Date()); // => true

		assertTrue(result);
		
		

		// - - -
		//
		// **isRegExp** `_.isRegExp(object)`
		//
		// Returns true if object is a RegExp.
		//
		
		var result =  _.isRegExp(/moe/); // => true
		
		assertTrue(result);




		// - - -
		//
		// **isNaN** `_.isNaN(object)`
		//
		// Returns true if object is NaN.
		//
		// **Note**: this is not the same as the native isNaN function, 
		// which will also return true if the variable is undefined.
		//

		var result = _.isNaN(NaN); // => true

		assertTrue(result);
		
		
		var result = isNaN(undefined); //  => true
		
		assertTrue(result);
		

		var result = _.isNaN(undefined); // => false

		assertFalse(result);
		
		

		var result = isNaN(NaN); // => true
		
		assertTrue(result);
		
		
		var result = isNaN(undefined); // => true
		
		assertTrue(result);






		// - - -
		//
		// **isNull** `_.isNull(object)`
		//
		// Returns true if the value of object is null.
		//
		
		var result = _.isNull(null); // => true
		
		assertTrue(result);
		
		
		var result =  _.isNull(undefined); // => false
		
		assertFalse(result);
		



		// - - -
		//
		// **isUndefined** `_.isUndefined(variable)`
		//
		// Returns true if variable is undefined.
		//

		var result = _.isUndefined(window.missingVariable); // => true
		
		assertTrue(result);
		
		
		var result = _.isUndefined(alert); // => false
		
		assertFalse(result);
		
		







		// - - -
		//
		// ### Utility Functions
		// 
		// - - -

		
		// - - -
		//
		//  **noConflict** `_.noConflict()`
		// 
		// Give control of the "_" variable back to its previous owner. 
		// Returns a reference to the Underscore object.
		//
		//
		// `var underscore = _.noConflict();`
		//
		// `assertTrue(underscore.isFunction(underscore.prototype.has));`
		//
		
		

		


		// - - -
		//
		// **identity** `_.identity(value)`
		//
		// Returns the same value that is used as the argument. In math: `f(x) = x`
		//
		// This function looks useless, but is used throughout Underscore as a default iterator.
		//

		var moe = {name : 'moe'};
		var result = moe === _.identity(moe); // => true
		
		assertTrue(result);
		
		

		// - - -
		//
		// **times** `_.times(n, iterator)`
		//
		// Invokes the given iterator function n times.
		//

		var result = 0;
		
		_(3).times(function(){ result++; });

		assertThat(result, equalTo(3));





		// - - -
		// 
		// **mixin** `_.mixin(object)`
		//
		// Allows you to extend Underscore with your own utility functions. 
		//
		// Pass a hash of `{name: function}` definitions to have your functions 
		// added to the Underscore object, as well as the OOP wrapper.
		//
		
		_.mixin({
		  capitalize : function(string) {
		    return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
		  }
		});
		
		var result = _("fabio").capitalize(); // => "Fabio"
		
		assertThat(result, equalTo("Fabio"));


		// - - -
		//
		// **uniqueId** `_.uniqueId([prefix])`
		//
		// Generate a globally-unique id for client-side models or DOM elements that need one. 
		//
		// If prefix is passed, the id will be appended to it.
		//
		
		var result =  _.uniqueId('contact_'); // => 'contact_104'

		assertThat(result.indexOf('contact_') >= 0);


		// `_(3).times(function() { jstestdriver.console.log(_.uniqueId());  });`


		// - - -
		//
		// **escape** `_.escape(string)`
		//
		// Escapes a string for insertion into HTML, replacing `&, <, >, ", ', and /` characters.
		//

		var result = _.escape('Curly, Larry & Moe'); // => "Curly, Larry &amp; Moe"

		assertThat(result, equalTo("Curly, Larry &amp; Moe"));





		// - - -
		//
		// **template** `_.template(templateString, [context])`
		//
		// Compiles JavaScript templates into functions that can be evaluated for rendering.
		//
		// Useful for rendering complicated bits of HTML from JSON data sources.
		// Template functions can both interpolate variables, using
		// `<%= ... %>`, as well as execute arbitrary JavaScript code,
		// with `<% ... %>`.
		//
		// If you wish to interpolate a value, and have it be HTML-escaped,
		// use `<%- ... %>`.
		//
		// When you evaluate a template function, pass in a context object
		// that has properties corresponding to the template's free variables.
		// If you're writing a one-off, you can pass the context object
		// as the second parameter to template in order to render
		// immediately instead of returning a template function.
		//


		var people = [
			{ fname: 'Atanas', lname: 'Hristov' },
			{ fname: 'John', lname: 'Doe' }
		];

		var tmpl = "";
		tmpl += "	<table cellspacing='0' cellpadding='0' border='1' id='employee'>        ";
		tmpl += "		<tbody>                                                             ";
		tmpl += "			<%                                                              ";
		tmpl += "				for (var i=0; i< people.length; i++)  {                     ";
		tmpl += "					var person = people[i];                                 ";
		tmpl += "			%>                                                              ";
		tmpl += "					<tr>                                                    ";
		tmpl += "						<td><%= person.fname %></td>                        ";
		tmpl += "						<td><%= person.lname %></td>                        ";
		tmpl += "					</tr>                                                   ";
		tmpl += "			<% 	                                                            ";
		tmpl += "				}                                                           ";
		tmpl += "			%>                                                              ";
		tmpl += "		</tbody>                                                            ";
		tmpl += "	</table>                                                                ";


		var result = _.template(tmpl, {people: people});
		assertTrue(result.indexOf('Atanas')>=0);







		// - - -
		//
		// ### Chaining Functions
		//
		// You can use Underscore in either an object-oriented or a functional style,
		// depending on your preference.
		//
		// The following two lines of code are identical ways to double a list of numbers.
		//
		//      _.map([1, 2, 3], function(n){ return n * 2; });
		//      _([1, 2, 3]).map(function(n){ return n * 2; });
		//

		var result1 = _.map([1, 2, 3], function(n){ return n * 2; });

		assertThat(result1, anyOf(hasSize(3), hasItems(2, 4, 6)));


		var result2 = _([1, 2, 3]).map(function(n){ return n * 2; });

		assertThat(result2, anyOf(hasSize(3), hasItems(2, 4, 6)));


		//
		// Using the object-oriented style allows you to chain together methods.
		// Calling chain on a wrapped object will cause all future method calls to return wrapped objects as well.
		// When you've finished the computation, use value to retrieve the final value.
		//
		// Here's an example of chaining together a map/flatten/reduce,
		// in order to get the word count of every word in a song.
		//
		// In addition, the Array prototype's methods are proxied through
		// the chained Underscore object, so you can slip a reverse or a push into your chain,
		// and continue to modify the array.
		//


		var lyrics = [
			{line : 1, words : "I'm a lumberjack and I'm okay"},
			{line : 2, words : "I sleep all night and I work all day"},
			{line : 3, words : "He's a lumberjack and he's okay"},
			{line : 4, words : "He sleeps all night and he works all day"}
		];

		_.chain(lyrics)
			.map(function(line) { return line.words.split(' '); })
			.flatten()
			.reduce(function(counts, word) {
				counts[word] = (counts[word] || 0) + 1;
				return counts;
			}, {}).value(); // => {lumberjack : 2, all : 4, night : 2 ... }



		// - - -
		//
		// **chain** `_.chain(obj)`
		//
		// Returns a wrapped object.
		// Calling methods on this object will continue to return
		// wrapped objects until value is used.
		//




















    }

	

});

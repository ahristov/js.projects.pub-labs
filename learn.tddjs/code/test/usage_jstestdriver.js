//
// [JsTestDriver](http://code.google.com/p/js-test-driver/) is a:
// 1. easily integrates with continuous builds systems and
// 2. allows running tests on multiple browsers quickly to ease TDD style development.
//

TestCase("Usage of JsTestDriver", {

	setUp : function() {
		/*:DOC += 
<div class="page">
	<div id="error">There has been an error on this page</div>
</div>		
		*/
		
		this.reEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	},
	
	tearDown : function() {
		delete this.reEmail;
	},

	"test - Usage of JsTestDriver" : function() {
		
		// ### Asserts
		//
		
		// **assertTrue**([msg], actual)  
		// Fails if the result isn't truthy. To use a message, add it as the first parameter.
		assertTrue("Should be true!", true);
		
		// **assertFalse**([msg], actual)  
		// Fails if the result isn't falsy.
		assertFalse("Should be false!", false);
		
		// **assertEquals**([msg], expected, actual)  
		// Fails if the expected and actual values can not be compared to be equal.
		assertEquals("Should be equal to", 1, 1);
		
		// **assertNotEquals**([msg], expected, actual)  
		// Fails if the expected and actual values can be compared to be equal.
		assertNotEquals("Should not be equal to", 1, 2);
		
		// **assertSame**([msg], expected, actual)  
		// Fails if the expected and actual values are not references to the same object.
		assertSame("Should reference same object", this, this);
		
		// **assertNotSame**([msg], expected, actual)  
		// Fails if the expected and actual are references to the same object.
		assertNotSame("Should not be same object", this, 1);
		
		// **assertNull**([msg], actual)  
		// Fails if the given value is not exactly null.
		assertNull("Should be null", null);
		
		// **assertNotNull**([msg], actual)  
		// Fails if the given value is exactly null.
		assertNotNull("Should not be null", this);
		
		// **assertUndefined**([msg], actual)  
		// Fails if the given value is not undefined.
		assertUndefined("Should be undefined", window.thisIsNotDefined);
		
		// **assertNotUndefined**([msg], actual)  
		// Fails if the given value is undefined.
		assertNotUndefined("Should not be undefined", this);
		
		// **assertNaN**([msg], actual)  
		// Fails if the given value is not a NaN.
		assertNaN("Should not be a number", "abc");
		
		// **assertNotNaN**([msg], actual)  
		// Fails if the given value is a NaN.
		assertNotNaN("Should be a number", "1");
		
		// **assertException**([msg], callback, error)  
		// Fails if the code in the callback does not throw the given error.
		/*
		assertException("Should throw an exception \"Error\"",
			function() { var foo = window.foo.bar; },
			"TypeError"
		);
		*/
		
		// **assertNoException**([msg], callback)  
		// Fails if the code in the callback throws an error.
		/*
		assertNoException("Should not throw \"TypeError\"",
			function() {},
			"TypeError"
		);
		*/
		
		// **assertArray**([msg], actual)  
		// Fails if the given value is not an Array.
		assertArray("Should be an array", [1,2,3]);
		
		// **assertTypeOf**([msg], expected, value)  
		// Fails if the JavaScript type of the value isn't the expected string.
		assertTypeOf("Should be a string", "string", "abc");
		
		// **assertBoolean**([msg], actual)  
		// Fails if the given value is not a Boolean. Convenience function to assertTypeOf.
		assertBoolean("Should be a boolean", true);
		
		// **assertFunction**([msg], actual)  
		// Fails if the given value is not a Function. Convenience function to assertTypeOf.
		assertFunction("Should be a function", function() {});
		
		// **assertObject**([msg], actual)  
		// Fails if the given value is not an Object. Convenience function to assertTypeOf.
		assertObject("Should be an object", new Object());
		
		// **assertNumber**([msg], actual)  
		// Fails if the given value is not a Number. Convenience function to assertTypeOf.
		assertNumber("Should be a number", Math.PI);
		
		// **assertString**([msg], actual)  
		// Fails if the given value is not a String. Convenience function to assertTypeOf.
		// **Note**: new String() will be threaded as an 'object'. 
		assertString("Should be a string", "abc");
		
		// **assertMatch**([msg], regexp, actual)  
		// Fails if the given value does not match the given regular expression.
		assertMatch("Should be a valid email address",
			this.reEmail,
			"email@host.com");
		
		// **assertNoMatch**([msg], regexp, actual)  
		// Fails if the given value matches the given regular expression.
		assertNoMatch("Should not be a valid email address",
			this.reEmail,
			"email-at-host.com");
		
		// **assertTagName**([msg], tagName, element)  
		// Fails if the given DOM element is not of given tagName.
		
		//	var errorDiv = document.createElement("div");
		//	errorDiv.id = "error";
		//	document.body.appendChild(errorDiv);
		
		assertTagName("The \"error\" element is a div", 
			"div", 
			document.getElementById("error"));
		
		// **assertClassName**([msg], className, element)  
		// Fails if the given DOM element does not have given CSS class name.
		
		
		// **assertElementId**([msg], id, element)  
		// Fails if the given DOM element does not have given ID.
		assertElementId("Should have ID 'error'", 'error', 
			document.getElementsByTagName('div')[1]);
		
		// **assertInstanceOf**([msg], constructor, actual)  
		// Fails if the given object is not an instance of given constructor.
		assertInstanceOf("Should be an instance of array", 
			Array, 
			new Array());
		
		// **assertNotInstanceOf**([msg], constructor, actual)  
		// Fails if the given object is an instance of given constructor.
		assertNotInstanceOf("Should not be an instance of array", 
			Array, 
			new String("foo"));
	
			
	}
	
});



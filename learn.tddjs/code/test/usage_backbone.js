TestCase("Backbone usage", {

	hook: null,
	
	setUp: function() {
		/*:DOC += 
<div class="page">
			
<div id="search_container"></div>
			
<script type="text/template" id="search_template">
	<label id="search_label"><%= search_label %></label>
	<input type="text" id="search_input" />
	<input type="button" id="search_button" value="Search" />
</script>			

</div>		
				*/

		Person = Backbone.Model.extend({
			
			// Somethimes we may need to set-up defaults.
			//
			
			defaults: {
				name: "",
				age: 0,
				children: []
			},
			
			// `initialize()` is triggered whenever you create a new instance 
			// of a model( models, collections and views work the same way ). 
			//
			// You don't have to include it in your model declaration but you 
			// will find yourself using it more often than not.
			//
			
			initialize: function() {
	
				// All attributes of a model can have listeners bound to them 
				// to detect changes to their values.
				//
				
				this.bind("change:name", function() {
					this.set({name: "Papa"});
				});
				
				
				// Bind on validation error function
				//
				
				this.bind("error", function(model, error){
					hook = error;
				});
			},
			
			// model validation:
			// If you return a string from the validate function,
			// Backbone will throw an error.
			//
			
			validate: function(attributes) {
				if (attributes.age < 0) {
					return "Age should be positive value.";
				}
			}

		});
		
		
		this.person = new Person({
			name: "Papa",
			age: 46,
			children: ["Son", "Daughter"]
		});
		
		// Passing a javascript object to our constructor is the same 
		// as calling model.set().
		//
		this.person.set({name: "Papa", age: 46});
		
		

		SearchView = Backbone.View.extend({
			
			initialize: function() {
				this.render();
			},
			
			render: function() {
	
				//Pass variables in using Underscore.js Template
				var variables = { search_label: "My Search" };
			
				// Compile the template using underscre
				var template = _.template( $('#search_template').html(), variables);
				
				// Load the compiled HTML into the Backbone "el"
				this.el.innerHTML = template;
			},
			
			events: {
	            "click input[type=button]": "doSearch"
	        },
	        
			doSearch: function( event ){
	        	hook = "Search for " + $("#search_input").val();
			}
			
		});

		
		this.search_view = new SearchView({ el: $("#search_container") });


	},
	
	tearDown: function() {
		delete this.person;
	},

	
	"test - Properties should be set" : function() {
		
		// Using the model.get() method we can access model properties at anytime.
	
	 	assertThat(this.person.get('name'), equalTo('Papa'));
	 	assertThat(this.person.get('age'), equalTo(46));
	 	assertThat(this.person.get('children'), anyOf(hasSize(2), hasItems("Son")));
	 	
	},

	"test - Can not change name" : function() {
	
		this.person.set({name: "Foo"});
		assertThat(this.person.get('name'), equalTo('Papa'));
		jstestdriver.console.log(this.person.get('name'));
	},

	"test - Can get copy of all current attributes" : function() {
	
		var p_tmp = new Person({name: "Name"});
		assertThat(p_tmp.get('name'), equalTo('Name'));

		var attributes = this.person.toJSON();
		
		// `attributes` gives direct access, it is a reference
		assertThat(attributes.name, equalTo('Papa'));
	},
	
	"test - Error message from validation" : function() {
		
		// When trying to set a property to value that brokes the rules for validation,
		// the property will remain in the old state.
		// It also will fire the "error" binded handler.
		//

		this.person.set({age: -1});
		jstestdriver.console.log(hook + ' ' + this.person.get('age'));
		assertThat(hook, equalTo("Age should be positive value."));
		assertThat(this.person.get('age'), equalTo(46));
		
	},

	"test - events on the template" : function() {
	
		$('#search_input').val('jsTestDriver')
		$('#search_button').click();
		
		assertThat(hook, equalTo("Search for jsTestDriver"));

		$('#search_label').html();
	},

	"test - variables on the template" : function() {
	
		assertThat($('#search_label').html(), equalTo("My Search"));
	}
	
});





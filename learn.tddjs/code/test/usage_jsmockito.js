// [JsMockito](http://jsmockito.org/) is a JavaScript stub/mock framework 
// heavily inspired by [Mockito](http://mockito.org/). 
//
// JsMockito aims to try and reproduce the clean & simple API. 
// 
// JsMockito must be served with [JsHamcrest](http://jshamcrest.destaquenet.com/index.html). 
// 
// To use JsMockito with a JavaScript unit test framework, follow the usual installation/configuration 
// instructions for the framework and plug JsMockito into it. 
// 
// If you're integrating with [Screw.Unit](http://github.com/nkallen/screw-unit/tree/master) (and why wouldn't you?) 
// then you just need to make the following calls:
// 
//			JsHamcrest.Integration.screwunit();
//			JsMockito.Integration.screwunit();
//
// #### Integration with JsTestDriver
// 
// Following is an example of integration with [JsTestDriver](http://code.google.com/p/js-test-driver/)
//
// 1. Create directory `test_deps` where you'll put files to load last from the jsTstDriver.conf.
// 2. Add file `jshamcrest_plugin.js` in this directory with the content:
//
//			JsHamcrest.Integration.JsTestDriver();
//			JsMockito.Integration.JsTestDriver();
//
// 3. Load `test_deps` from our jsTestDriver.conf file as last entry:
//
//		server: http://localhost:42442
//				
//			load:
//			 - src_deps/*.js
//			 - src/*.js
//			 - test_deps/*.js
//			 - test/*.js
//			 - test_plugins/*.js
//

TestCase("Usage of JsMoskito", {

	"test - Usage of JsMoskito" : function() {

		// - - - 
		//
		// ### Verify iteractions
		//

	    var mockedObj = mock(Array);
	    mockedObj.push("one");
	    mockedObj.push("two");
	    verify(mockedObj).push("one"); // got exactly ONE call to push() with parameter "one".
	    verify(mockedObj).push("two"); // got exactly ONE call to push() with parameter "two".

	    
		// - - - 
		//
		// ### Stub a method call
		//

	    var mockedObj = mock(Array);
	    when(mockedObj).pop().thenReturn("hello world");

	    assertThat(mockedObj.pop(), equalTo("hello world"));
	    assertThat(mockedObj.pop(), equalTo("hello world"));
	    assertThat(mockedObj.pop(), equalTo("hello world"));
	    assertThat(mockedObj.pop(), equalTo("hello world"));
	    assertThat(mockedObj.pop(), equalTo("hello world"));

	    

		// - - - 
		//
	    // ### Mock functions
	    //

	    var mockFunc = mockFunction();
	    when(mockFunc)(anything()).then(function(arg) {
	      return "foo " + arg;
	    });
	  
	    mockFunc("bar");

	    verify(mockFunc)(anything());
	
	    // or if you want to verify the scope it was called with, use:
	    // `verify(mockFunc).call(this, anything())`

	    

	    
		// - - - 
		//
	    // ### Spy objects
	    //
	    
	    var realObj = new Array();
	    var mockedObj = spy(realObj);
	
	    when(mockedObj).pop().thenReturn("bar"); // mock pop()
	
	    mockedObj.push("foo");
	    assertThat(realObj.length, equalTo(1));
	    
	    mockedObj.pop();
	    assertThat(realObj.length, equalTo(1)); // the realObj untouched. pop()was mocked.
	    
	    verify(mockedObj).push("foo");
	    verify(mockedObj).pop();




		// - - - 
		//
	    // ### Spy functions
	    //
	    
	    var realFunc = function(msg) { jstestdriver.console.log("called realFunc"); };
	    var mockFunc = spy(realFunc);
	    mockFunc("hello world");
	    verify(mockFunc)("hello world");
	    
	    





	}
	
});

TestCase("Date.strftime test", {
	
	setUp: function() {
		this.date = new Date(2009, 9, 2, 22, 14, 45);
	},
	
	tearDown: function() {
		delete this.date;
	},
	
	"test %Y should return full year" : function() {
		
		jstestdriver.console.log("This is a log message");
		
		var year = Date.formats.Y(this.date);
		
		assertNumber(year);
		assertEquals(2009, year);
                
                var d = new Date();
                
	}

});


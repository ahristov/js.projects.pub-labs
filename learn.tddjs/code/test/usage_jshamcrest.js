//
// [JsHamcrest](http://jshamcrest.destaquenet.com/index.html) is a JavaScript library 
// heavily inspired by [Hamcrest](http://code.google.com/p/hamcrest/). 
//
// It provides a large library of matcher objects (also known as constraints or predicates) 
// allowing “match” rules to be defined declaratively. Typical scenarios include testing frameworks, 
// mocking libraries, UI validation rules and object querying.
// 
// ### Integration with jsTestDriver 
// 
// 1. Let’s assume your project root directory have a `lib` directory to keep your 
// project’s dependencies. In this case, copy the `jshamcrest.js` file to that directory;
// 
// 2. Create a file `plugin/jshamcrest-plugin.js` in your project root directory 
// and put one of the following lines inside it:
//
//		JsHamcrest.Integration.JsTestDriver();
//		
//		// Same as above
//		JsHamcrest.Integration.JsTestDriver({
//		    scope:this
//		);
//
// 3. Finally, edit the jsTestDriver.conf file as follows:
//
//		load:
//		  - lib/*.js
//		  - <source directory>
//		  - <test cases directory>
//		  - plugin/*.js
//

TestCase("Usage of JsHamcrest", {

	"test - Usage of JsHamcrest" : function() {

		// ### Basic Usage
		//

		// Usage with the jsTestDriver's **assertTrue**() function:
		//
		assertTrue( equalTo('10').matches(10) );
		assertTrue( between(5).and(10).matches(7) );
		assertTrue( greaterThan(Math.PI).matches(3.1516) );
		
		// Usage with JsHamcrest's **assertThat**() function:
		//
		assertThat(10, equalTo(10) );
		assertThat(7,  between(5).and(10) );
		assertThat(3.1516,  greaterThan(Math.PI) );
		
		// ### Collection Matchers
		//
		
		
		// JsHamcrest.Matchers.**empty()**
		// The length of the actual value must be zero.
		//
		assertThat([], empty());
		
		// JsHamcrest.Matchers.**everyItem**(matcherOrValue)
		// The actual value should be an array and matcherOrValue must match all items:
		//
		assertThat([1,2,3], everyItem(greaterThan(0)));
		assertThat([1,'1'], everyItem(1));
		
		// JsHamcrest.Matchers.**hasItem**(matcherOrValue)
		// The actual value should be an array and it must contain at least one value that matches matcherOrValue:
		//
		assertThat([1,2,3], hasItem(equalTo(3)));
		assertThat([1,2,3], hasItem(3));
		
		// JsHamcrest.Matchers.**hasItems**(MatchersOrValues...)
		// The actual value should be an array and matchersOrValues must match at least one item:
		//
		assertThat([1,2,3], hasItems(2,3));
		assertThat([1,2,3], hasItems(greaterThan(2)));
		assertThat([1,2,3], hasItems(1, greaterThan(2)));
		
		// JsHamcrest.Matchers.**hasSize**(matcherOrValue)
		// The length of the actual value value must match matcherOrValue:
		//
		assertThat([1,2,3], hasSize(3));
		assertThat([1,2,3], hasSize(lessThan(5)));

		// JsHamcrest.Matchers.**isIn**(item...)
		// The given array or arguments must contain the actual value:
		//
		assertThat(1, isIn([1,2,3]));
		assertThat(1, isIn(1,2,3));		
		
		
		// ### Core Matchers
		//
		
		// JsHamcrest.Matchers.**allOf**(matchersOrValues...)
		// All matchesOrValues must match the actual value. This matcher behaves pretty much like the JavaScript && (and) operator:

		assertThat(5, allOf([greaterThan(0), lessThan(10)]));
		assertThat(5, allOf([equalTo(5), lessThan(10)]));
		assertThat(5, allOf([greaterThan(0), lessThan(10)]));
		assertThat(5, allOf([equalTo(5), lessThan(10)]));


		// JsHamcrest.Matchers.**anyOf**(matchersOrValues)
		// At least one of the matchersOrValues should match the actual value. This matcher behaves pretty much like the || (or) operator:
		
		assertThat(5, anyOf([even(), greaterThan(2)]));
		assertThat(5, anyOf(even(), greaterThan(2)));
		
		// JsHamcrest.Matchers.**anything**()
		// Useless always-match matcher:
		
		assertThat('string', anything());
		assertThat(null, anything());
		
		// JsHamcrest.Matchers.**both**(matcherOrValue)
		// Combinable matcher where the actual value must match all the given matchers or values:
		
		assertThat(10, both(greaterThan(5)).and(even()));
		
		// JsHamcrest.Matchers.**either**(matcherOrValue)
		// Combinable matcher where the actual value must match at least one of the given matchers or values:
		
		assertThat(10, either(greaterThan(50)).or(even()));
		
		// JsHamcrest.Matchers.**equalTo**(expected)
		// The actual value must be equal to expected:
		
		assertThat('10', equalTo(10));
		
		// JsHamcrest.Matchers.**is**(matcherOrValue)
		// Delegate-only matcher frequently used to improve readability:
		
		assertThat('10', is(10));
		assertThat('10', is(equalTo(10)));
		
		// JsHamcrest.Matchers.**nil**()
		// The actual value must be null or undefined:
		
		var undef;
		assertThat(undef, nil());
		assertThat(null, nil());
		
		// JsHamcrest.Matchers.**not**(matcherOrValue)
		// The actual value must not match matcherOrValue:
		
		assertThat(10, not(20));
		assertThat(10, not(equalTo(20)));
		
		// JsHamcrest.Matchers.**raises**(exceptionName)
		// The actual value is a function and, when invoked, it should thrown an exception with the given name:
		
		var MyException = function(message) {
		    this.name = 'MyException';
		    this.message = message;
		};
		
		var myFunction = function() {
		    // Do something dangerous...
		    throw new MyException('Unexpected error');
		};
		
		assertThat(myFunction, raises('MyException'));
		
		
		// JsHamcrest.Matchers.**sameAs**(expected)
		// The actual value must be the same as expected:
		
		var number = 10, anotherNumber = number;
		assertThat(number, sameAs(anotherNumber));
		
		// JsHamcrest.Matchers.**truth**()
		// The actual value must be any value considered truth by the JavaScript engine:
		
		var undef;
		assertThat(10, truth());
		assertThat({}, truth());
		assertThat(0, not(truth()));
		assertThat('', not(truth()));
		assertThat(null, not(truth()));
		assertThat(undef, not(truth()));
		
		
	}
	
});

AsyncTestCase("Tdd.benchmark async test", {
	
	setUp: function() {
		this.loopLength = 5;
		this.array = [];
		for (var i = 0; i < this.loopLength; i++) {
			this.array[i] = "item" + i;
		}
	},
	
	tearDown: function() {
		delete this.array;
		delete this.loopLength;
	}

/*	
	"test benchmark adds div with ID tdd.benchmark to page" : function() {

		
		Tdd.benchmark("Loop performance", {
			"for-loop" : function() {
				for (var i = 0, item; i < this.loopLength; i++) {
					item = this.array[i];
				}
			}
		},5);

		setTimeout(function() {
			assertNotNull(document.getElementById('tdd.benchmark'));
			assertNotUndefined(document.getElementById('tdd.benchmark'));
			assertTrue(document.getElementById('tdd.benchmark').innerHTML.indexOf('for-loop') >= 0);
		},30);

	}
*/

});



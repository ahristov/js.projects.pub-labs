TestCase("Tdd.assert test", {

	"test Tdd.assert is a function" : function() {

		assertNotNull(Tdd.assert);
		assertTypeOf("function", Tdd.assert);
	},


	"test Tdd.assert on success should increase count of passed asserts" : function() {

		Tdd.assertResetCountPassed();
		Tdd.assert("true is true", true);

		assertEquals(1, Tdd.assertCountPassed());
	},
	
	"test Tdd.assert can reset count of passed asserts" : function() {

		Tdd.assert("true is true", true);
		Tdd.assertResetCountPassed();
		
		assertEquals(0, Tdd.assertCountPassed());
		
	}
	
/*	
	"test Tdd.assert throws exception" : function() {

		assertException(function () { Tdd.assert("False is false", false); }, 
			"Error"");
		
	}
*/
	
});

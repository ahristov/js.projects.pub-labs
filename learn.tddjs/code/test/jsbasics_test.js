//
// Basic JS concepts
//



TestCase("Basic JS", {


	"test - Inheritance" : function() {

			// ### Inheritance
			//

			// Having a Function object
			//

			function Foo() { 

				// We can define properties and methods
				// for this Function object referencing `this`.
				//
				this.name = 'Bar'; 

				this.log = function () { 
					console.log(this.name); 
				};

			}

			var foo = new Foo();

			assertThat(foo.name, equalTo("Bar"));
			assertUndefined(foo.length);

			// We can borrow in the prototype all the functionality 
			// from another object which we instantiate, see `new Array()`.
			//
		
			Foo.prototype = new Array();

			// after re-instantiate the variable we'll have the functionality from `Array`.
			//
			var foo = new Foo();
			foo.push(123);

			assertThat(foo.name, equalTo("Bar"));
			assertNotUndefined(foo.length);
			
			assertThat(foo.length, equalTo(1));
			assertThat(foo[0], equalTo(123));
	},



	"test - call and apply" : function() {

			// ### call() and aply()
			// 

			// Somethimes we just wanna borrow one method from another object.
			// In this case we don't need to inherit everything.

			// call's first param is the object that borrows functionality.
			// call's rest of params are variable list of params to the called function.
			//
			var result = "not my object".indexOf.call("my object", "my");
			assertThat(result, equalTo(0));


			// apply's first param is the object that borrows functionality.
			// apply's second param is an array of the params to send to the called function.
			//
			var result = "not my object".indexOf.apply("my object", ["my"]);
			assertThat(result, equalTo(0));

	}






	
});

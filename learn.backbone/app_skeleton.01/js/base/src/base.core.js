define(["underscore", "jquery"],

	/**
	 * Base Core.
	 *
	 * Encapsulates the core functionality and all dependencies on external libraries.
	 * Implements the `mediator` design pattern.
	 * The modules don't directly use this class, but they use the sandbox instead.
	 *
	 */
	function (_, $) {

		var channels = {}, 		// Loaded channels and their callbacks
			base_core = {};		// Mediator object


		/**
		 * Gets all the channels.
		 */
		base_core.getChannels = function() {
			return channels;
		};

		/**
		 * Subscrive to an event.
		 *
		 * @param {String} channel Event name
		 * @param {Function} callback Module callback
		 * @param {Object} context Context in which to execute the module
		 */
		base_core.subscribe = function(channel, callback, context) {

			if (channel === undefined) {
				throw new Error("Channel must be defined!");
			}

			if (callback === undefined) {
				throw new Error("Callback must be defined!");
			}

			if (context === undefined) {
				throw new Error("Context must be defined!");
			}

			channels[channel] = (channels[channel])
				? channels[channel]
				: [];

			channels[channel].push(callback.bind(context));

		};

		/**
		 * Publish an event, passing arguments to subscribers.
		 * Will call start if the channel is not already registered.
		 *
		 * @param {String} channel Event name
		 */
		base_core.publish = function(channel) {

			var i, l, args = [].slice.call(arguments, 1);

			if (!channels[channel]) {
				base_core.start.apply(this, arguments);
				return;
			}

			for (i = 0, l = channels[channel].length; i < l; i += 1) {
				channels[channel][i].apply(this, args);
			}
		};


		/**
		 * Automatically load a widget and initialize it.
		 *
		 * File name of the widget will be derived from the channel,
		 * decamelized and underscore delimited by default.
		 *
		 * @param {String} channel Event name
		 */
		base_core.start = function(channel) {

			var i, l,
				args = [].slice.call(arguments, 1),
				file = base_core.utils.decamelize(channel);

			// If the widget hasn't called subscribe
			// this will fail because it won't
			// be present in the channels object.
			require(["widgets/" + file + "/main"],
				function() {
					for (i = 0, l = channels[channel].length; i < l; i += 1) {
						channels[channel][i].apply(base_core, args);
					}
				}
			);

		};


		/**
		 * Unload a widget (collection of modules) by passing in a named reference
		 * to he channel/widget.
		 *
		 * This will both locate and reset the internal state of the modules
		 * in require.js and empty the widget's DOM element.
		 *
		 * @param {String} channel Event name
		 */
		base_core.stop = function(channel) {

			var args = [].slice.call(arguments, 1),
				el = args[0],
				file = base_core.utils.decamelize(channel);

			// Remove all modules under a widget path
			// (e.q widget/todoslist, widget/edittodo)
			base_core.unload("widgets/" + file);

			// Empty markup assosiated with the module
			$(el).html();

		};


		/**
		 * Undefine/unload module, reseting the internal state of it in require.js
		 * to act like it wasn't loaded. By default require won't cleanup any markup
		 * assosiated with it.
		 *
		 * We'll just remove those modules that fall under the widget path as we know
		 * those dependencies (e.g. models, views, etc) should only
		 * belong to one part of the codebase and shouldn't depend on by others.
		 *
		 * We are not removing shared dependencies. E.g. say on loaded up a module
		 * containing jQuery, others also use jQuery. We don't have track of dependencies
		 * for the shared modules.
		 *
		 * @param {String} channel Event name
		 */
		base_core.unload = function(channel) {

			var contextMap = requirejs.s.contexts._.urlMap, key;

			for (key in contextMap) {
				if (contextMap.hasOwnProperty(key) && key.indexOf(channel) !== -1) {
					require.undef(key);
				}
			}

		};


		/**
		 * Contains utility methods.
		 */
		base_core.utils = {

			each: _.each,

			/**
			 * Decamelize camelized string
			 *
			 * @param {String} camelCase camelized string
			 * @param {String} delimiter The string to use between the parts: "", "_" (default), etc.
			 */
			decamelize: function(camelCase, delimiter) {
				delimiter = (delimiter === undefined) ? "_" : delimiter;
				return camelCase.replace(/([A-Z])/g, delimiter + '$1').toLowerCase();
			},

			/**
			 * Camelize the given string
			 *
			 * @param {String} str The string to camelize.
			 */
			camelize: function (str) {
				return str.replace(/(?:^|[\-_])(\w)/g, function (delimiter, c) {
					return c ? c.toUpperCase() : '';
				});
			}

		};


		/**
		 * Contains DOM related methods.
		 */
		base_core.dom = {

			/**
			 * Get the descendants of each element in the current set of matched elements,
			 * filtered by CSS selector.
			 *
			 * By default, selectors perform their searches within the DOM starting at the document root.
			 * However, an alternate context element can be given for the search by using the optional second parameter.
			 *
			 * @param {String|HTMLElement} selector A string containing a selector expression to match elements against.
			 * @param {HTMLElement} context (opt) Context element.
			 */
			find: function(selector, context) {

				context = context || document;

				if (typeof selector === "string") {
					return $(context).find(selector);
				} else {
					return $(context).find($(selector));
				}

			},

			/**
			 * Insert content, specified by the parameter, to the end of each element in the set of matched elements.
			 *
			 * @param {String} content DOM element, HTML string, or jQuery object to insert at the end of each element in the set of matched elements.
			 * @param {String|HTMLElement} selector A string containing a selector expression to match elements against.
			 * @param {HTMLElement} context (opt) Context element.
			 */
			append: function(content, selector, context) {
				this.find(selector, context).append(content);
			},

			/**
			 * Adds the specified class(es) to each of the set of matched elements.
			 *
			 * @param {String} className One or more class names to be added to the class attribute of each matched element.
			 * @param {String|HTMLElement} selector A string containing a selector expression to match elements against.
			 * @param {HTMLElement} context (opt) Context element.
			 */
			addClass: function(className, selector, context) {
				this.find(selector, context).addClass(className);
			},

			/**
			 * Remove a single class, multiple classes, or all classes from each element in the set of matched elements
			 *
			 * @param {String} className One or more class names to be added to the class attribute of each matched element.
			 * @param {String|HTMLElement} selector A string containing a selector expression to match elements against.
			 * @param {HTMLElement} context (opt) Context element.
			 */
			removeClass: function(className, selector, context) {
				this.find(selector, context).removeClass(className);
			}

		};



		/**
		 * Contains event handling methods.
		 */
		base_core.events = {

			/**
			 * Attach an event handler function for one or more events to the selected elements.
			 *
			 * @param {String|HTMLElement} context String CSS selector or html element
			 * @param {String} events One or more space-separated event types such as "click" or "keydown".
			 * @param {Function} callback A function to execute when the event is triggered.
			 */
			listen: function (context, events, callback) {
				return $(context).on(events, callback);
			}


		};


		/**
		 * Contains template parsing modules.
		 */
		base_core.template = {
			parse: _.template
		};

		/**
		 * A placeholder for things like ajax and local storage
		 */
		base_core.data = {

			ajax: {
				/**
				 * Load data from the server using a HTTP GET request.
				 *
				 * @param {String} url A string containing the URL to which the request is sent.
				 * @param {Function} success(data, textStatus, jqXHR) A callback function that is executed if the request succeeds.
				 * @param {String} dataType (opt) The type of data expected from the server. Default: Intelligent Guess (xml, json, script, text, html).
				 */
				get: function(url, success, dataType) {
					$.get(url, success, dataType);
				},

				/**
				 * Load data from the server using a HTTP POST request.
				 *
				 * @param {String} url A string containing the URL to which the request is sent.
				 * @param {Object} data A map or string that is sent to the server with the request.
				 * @param {Function} success(data, textStatus, jqXHR)A callback function that is executed if the request succeeds.
				 * @param {String} dataType (opt) The type of data expected from the server. Default: Intelligent Guess (xml, json, script, text, html).
				 */
				post: function(url, data, success, dataType) {
					$.post(url, data, success, dataType);
				}
			},

			/**
			 * The concrete storage class will be loaded from the `ext_core`.
			 */
			Store: undefined

		};


		/**
		 * The concrete MVC framework will be loaded from the `ext_core`.
		 */
		base_core.mvc = undefined;


		return base_core;
	}
);
define(["base_core"],

	/**
	 * Base Sandbox.
	 *
	 * Contains standard interface for modules.
	 * Implements the `facade` design pattern.
	 * This is a subset of the `base_core` functionality.
	 *
	 * @param {Object} base_core The Base Core object.
	 */
	function(base_core) {

		var base_sandbox = {};

		/**
		 * Subscrive to an event.
		 *
		 * @param {String} subscriber Module name. TODO: check for permissions.
		 * @param {String} channel Event name
		 * @param {Function} callback Module callback
		 */
		base_sandbox.subscribe = function(subscriber, channel, callback) {
			base_core.subscribe(channel, callback, this);
		};

		/**
		 * Publish an event, passing arguments to subscribers.
		 *
		 * @param {String} channel Event name
		 */
		base_sandbox.publish = function(channel) {
			base_core.publish.apply(base_core, arguments);
		};


		/**
		 * Automatically load a widget and initialize it.
		 *
		 * @param {String} channel Widget name
		 */
		base_sandbox.start = function(channel) {
			base_core.start.apply(base_core, arguments);
		};


		/**
		 * Unload a widget (collection of modules) by passing in a named reference
		 * to the channel/widget.
		 *
		 * @param {String} channel Widget name
		 */
		base_sandbox.stop = function(channel) {
			base_core.stop.apply(base_core, arguments);
		};



		/**
		 * Contains utility methods.
		 */
		base_sandbox.utils = {
			each: base_core.utils.each
		};


		/**
		 * Contains DOM related methods.
		 */
		base_sandbox.dom = {

			/**
			 * Get the descendants of each element in the current set of matched elements,
			 * filtered by CSS selector.
			 *
			 * By default, selectors perform their searches within the DOM starting at the document root.
			 * However, an alternate context element can be given for the search by using the optional second parameter.
			 *
			 * @param {String|HTMLElement} selector A string containing a selector expression to match elements against.
			 * @param {HTMLElement} context (opt) Context element.
			 */
			find: function(selector, context) {
				return base_core.dom.find(selector, context);
			},

			/**
			 * Insert content, specified by the parameter, to the end of each element in the set of matched elements.
			 *
			 * @param {String} content DOM element, HTML string, or jQuery object to insert at the end of each element in the set of matched elements.
			 * @param {String|HTMLElement} selector A string containing a selector expression to match elements against.
			 * @param {HTMLElement} context (opt) Context element.
			 */
			append: function(content, selector, context) {
				return base_core.dom.append(content, selector, context);
			},

			/**
			 * Adds the specified class(es) to each of the set of matched elements.
			 *
			 * @param {String} className One or more class names to be added to the class attribute of each matched element.
			 * @param {String|HTMLElement} selector A string containing a selector expression to match elements against.
			 * @param {HTMLElement} context (opt) Context element.
			 */
			addClass: function(className, selector, context) {
				return base_core.dom.addClass(className, selector, context);
			},

			/**
			 * Remove a single class, multiple classes, or all classes from each element in the set of matched elements
			 *
			 * @param {String} className One or more class names to be added to the class attribute of each matched element.
			 * @param {String|HTMLElement} selector A string containing a selector expression to match elements against.
			 * @param {HTMLElement} context (opt) Context element.
			 */
			removeClass: function(className, selector, context) {
				return base_core.dom.removeClass(className, selector, context);
			}

		};


		/**
		 * Contains event handling methods.
		 */
		base_sandbox.events = {

			/**
			 * Attach an event handler function for one or more events to the selected elements.
			 *
			 * @param {String|HTMLElement} context String CSS selector or html element
			 * @param {String} events One or more space-separated event types such as "click" or "keydown".
			 * @param {Function} callback A function to execute when the event is triggered.
			 */
			listen: function (context, events, callback) {
				base_core.events.listen(context, events, callback);
			}
		};


		/**
		 * Contains template parsing modules.
		 */
		base_sandbox.template = base_core.template;


		return base_sandbox;
	}
);
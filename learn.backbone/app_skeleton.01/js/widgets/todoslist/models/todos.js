define([
	"sandbox",
	"./todo"
],
function(sandbox, Todo) {

	var TodosCollection = sandbox.mvc.Collection({

		// Reference to the collection's model
		model: Todo,

		// Save all of the todo items under the `todos` namespace.
		localStorage: new sandbox.data.Store("todos"),

		// Filter down the list of all todos that are completed
		completed: function() {
			return this.filter(function(todo) {
					return todo.get("completed");
			});
		},

		// Filter down the list to only items that are still not finished.
		remaining: function() {
			return this.without.apply(this, this.completed());
		},

		// We keep the Todos in sequential order,
		// despite being saved by unordered GUID in the database.
		// This generates the next order number for new items.
		nextOrder: function() {
			if (!this.length) return 1;
			return this.last().get("order") + 1;
		},

		// Todos are sorted by their original insertion order
		comparator: function(todo) {
			return todo.get("order");
		}
	});


	return new TodosCollection;
});
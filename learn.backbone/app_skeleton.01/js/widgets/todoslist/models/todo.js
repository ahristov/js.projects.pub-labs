define([
	"sandbox"
],
function(sandbox) {

	var TodoModel = sandbox.mvc.Model({

		// Default attributes for the todo.
		defaults: {
			title: "",
			completed: false
		},

		// Ensure that each todo created has `title`.
		// Q: Can't we achieve this with validation???
		//
		initialize: function() {
			if (! this.get("title")) {
				this.set({"title": this.defaults.title});
			}
		},

		// Toggle the `completed` state of this todo item.
		toggle: function() {
			this.save({completed: !this.get("completed")});
		},

		// Remove this TODO from *localStorage*.
		clear: function() {
			this.destroy();
		}

	});

	return TodoModel;
});
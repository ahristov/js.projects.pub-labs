define([
	"sandbox",
	"../models/todos",
	"./todo",
	"text!../tmpl/app.html",
	"text!../tmpl/stats.html"
],
function(sandbox, TodosModel, TodoView, appTemplate, statsTemplate) {

	var AppView = sandbox.mvc.View({

		appTemplate : sandbox.template.parse(appTemplate),

		statsTemplate: sandbox.template.parse(statsTemplate),

		events: {
			"keypress #new-todo": "createOnEnter",
			"click #clear-completed": "clearCompleted",
			"click #toggle-all": "toggleAllComplete"
		},

		initialize: function() {
			this.el.innerHTML = appTemplate;
			this.input = sandbox.dom.find("#new-todo", this.el)[0];
			this.allCheckbox = sandbox.dom.find("#toggle-all", this.el)[0];

			this.newAttributes.bind(this);

			TodosModel.bind("add", this.addOne, this);
			TodosModel.bind("reset", this.addAll, this);
			TodosModel.bind("all", this.render, this);

			TodosModel.fetch();
		},

		// Re-rendering the App just means refreshing the statistics -- the rest
		// of the app doesn't change.
		render: function() {
			var completed = TodosModel.completed().length;
			var remaining = TodosModel.remaining().length;

			var html = this.statsTemplate({
				total		: TodosModel.length,
				completed	: completed,
				remaining	: remaining
			});

			var footerEl = sandbox.dom.find("#footer", this.el)[0];
			footerEl.innerHTML = html;

			this.allCheckbox.checked = !remaining;
		},

		// Add a single todo item to the list by creating a view for it, and
		// appending its element to the `<ul>`.
		addOne: function(todo) {
			var view = new TodoView({model: todo});
			sandbox.dom.append(view.el, "#todo-list", this.el);
		},

		// Add all items in the **Todos** collection at once.
		addAll: function() {
			TodosModel.each(this.addOne);
		},


		// Generate the attributes for a new Todo item.
		newAttributes: function() {
			return {
				title		: this.input.value,
				order		: TodosModel.nextOrder(),
				completed	: false
			};
		},


		// If you hit return in the main input field, create new **Todo** model,
		// persisting it to *localStorage*.
		createOnEnter: function(e) {
			if (e.keyCode != 13) return;
			if (!this.input.value) return;
			TodosModel.create(this.newAttributes());
			this.input.value = "";
		},

		// Clear all compelted todo items, destroying their models.
		clearCompleted: function() {
			sandbox.utils.each(TodosModel.completed(), function(todo){ todo.clear(); });
			return false;
		},


		// Change each todo so that it's `completed` state matches the check all
		toggleAllComplete: function () {
			var completed = this.allCheckbox.checked;
			TodosModel.each(function (todo) { todo.save({'completed': completed}); });
		}


	});

	return AppView;
});
define([
	"sandbox",
	"text!../tmpl/todo.html"
],
function(sandbox, todoTemplate) {

	var todoView = sandbox.mvc.View({

		// a list tag
		tagName: "li",

		// Cache the template function for a single item.
		template: sandbox.template.parse(todoTemplate),

		// The DOM events specific to an item
		events: {
			"click .toggle"		: "toggleCompleted",
			"dblclick .view"	: "edit",
			"click .destroy"	: "clear",
			"keypress .edit"	: "updateOnEnter",
			"blur .edit"		: "close"
		},

		// Listen for changes to the model.
		initialize: function() {
			this.model.bind("change", this.render, this);
			this.model.bind("destroy", this.remove, this);
		},

		// Re-rener the content when changed.
		render: function() {
			this.el.innerHTML = this.template(this.model.toJSON());
			this.input = sandbox.dom.find(".edit", this.el)[0];
		},

		// Toggle the `completed` state of the model.
		toggleCompleted: function() {
			this.model.toggle();
		},

		// Switch this view into `editing` mode, displaying the input field.
		edit: function() {
			sandbox.dom.addClass("editing", this.el);
			this.input.focus();
		},

		// Close the `editing` mode, saving changes to the todo.
		close: function() {
			var value = this.input.value;
			if (! value) { this.clear(); }

			this.model.save({title: value});
			sandbox.dom.removeClass("editing", this.el);
		},

		// If you hit `enter`, we're through editing the item.
		updateOnEnter: function(e) {
			if (e.keyCode == 13) { this.close(); }
		},

		// Remove the item, destroy the model
		clear: function() {
			this.model.clear();
		}



	});


	return todoView;

});
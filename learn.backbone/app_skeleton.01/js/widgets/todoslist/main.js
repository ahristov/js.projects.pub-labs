define([
	"sandbox",
	"./views/app",
	"text!./css/todoslist.css"
],
function(sandbox, AppView) {
	return sandbox.subscribe("bootstrap", "todoslist", function(element) {
		new AppView({el: sandbox.dom.find(element)});
	});
});
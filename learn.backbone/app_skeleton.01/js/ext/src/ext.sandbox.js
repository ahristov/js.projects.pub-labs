define(["base_sandbox", "core"], 
	function(base_sandbox, core) {
		
		var sandbox = Object.create(base_sandbox);

		sandbox.data = core.data;



		sandbox.mvc = {};

		sandbox.mvc.View = function(view) {
			return core.mvc.View.extend(view);
		};

		sandbox.mvc.Model = function(model) {
			return core.mvc.Model.extend(model);
		};

		sandbox.mvc.Collection = function(collection) {
			return core.mvc.Collection.extend(collection);
		};




		sandbox.widgets = {};

		sandbox.widgets.stop = function(channel, el) {
			return base_sandbox.stop.apply(this, arguments);
		};

		sandbox.widgets.start = function(channel, el) {
			return base_sandbox.start.apply(this, arguments);
		};


		return sandbox;
	}
);
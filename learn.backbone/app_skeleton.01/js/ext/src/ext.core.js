define(["base_core", "backbone", "localstorage"],
	function(base_core, Backbone, Store) {

		var core = Object.create(base_core);

		core.data.Store = Store;
		core.mvc = Backbone;

		return core;
	}
);
require.config({

	shim: {
		"underscore": {
			exports: "_"
		},

		"backbone": {
			deps: ["json", "underscore", "jquery"],
			exports: "Backbone"
		},

		"backbone_localstorage": {
			deps: ["backbone"]
		}

	},

	paths: {

		"text"					: "../vendor/require_text-2.0.0.min",
		"json"					: "../vendor/json-2.min",
		"underscore"			: "../vendor/underscore-1.3.3.min",
		"jquery"				: "../vendor/jquery-1.7.2.min",

		"base_core"				: "base/src/base.core",
		"base_sandbox"			: "base/src/base.sandbox",

		"backbone"				: "../vendor/backbone-0.9.2.min",
        "backbone_localstorage"	: "../vendor/backbone.localStorage-1.0.min",
		"jquery_ui"				: "../vendor/jquery-ui-1.8.21/js/jquery-ui-1.8.21.min",
		"bootstrap"				: "../vendor/bootstrap-2.0.4/js/bootstrap.min",
		"bootstrap_alert"		: "../vendor/bootstrap-2.0.4/js/bootstrap-alert",
		"bootstrap_dorpdown"	: "../vendor/bootstrap-2.0.4/js/bootstrap-dropdown",

		"core"					: "ext/src/ext.core",
		"sandbox"				: "ext/src/ext.sandbox"

	}
});

// Check if running test specs, do not start the widgets.
if (window.location.href.indexOf("test.html") < 0) {

	// Start with core modules
	requirejs(["core"], function (core) {

		// set fake userdata cookie
		//
		var userdataCookie = ""
			+ (((1+Math.random())*0x10000)|0).toString(16).substring(1)
			+ "-"
			+ (((1+Math.random())*0x10000)|0).toString(16).substring(1)
			+ "-"
			+ (((1+Math.random())*0x10000)|0).toString(16).substring(1)
			+ "-"
			+ (((1+Math.random())*0x10000)|0).toString(16).substring(1)
			+ ";"
			+ "Mr. Abc" + (((1+Math.random())*0x10000)|0).toString(16).substring(1)
		core.cookie.setCookie("userdata", userdataCookie, 1);


		//////////////////////////////////////////////////////////

		// load widgets here, one row per widget:
		// core.publish("widgetname", "#containername");
		//

		core.publish("logininfo", "#logininfo");

	});

}

define([
	"base_core",
	"backbone",
	"backbone_localstorage"
],

/**
 * Extended Core.
 *
 * Encapsulates the core functionality and all dependencies on external libraries.
 * Implements the `mediator` design pattern.
 * The modules don't directly use this class, but they use the sandbox instead.
 *
 *
 * @class App.Ext.Core
 * @extends App.Base.Core
 */
function(base_core, Backbone, Store) {

	var core = Object.create(base_core);


    /**
     * Contains utility methods.
     *
     * @class App.Ext.Core.utils
     * @extends App.Base.Core.utils
     */

    /**
     * Contains DOM related methods.
     *
     * @class App.Ext.Core.dom
     * @extends App.Base.Core.dom
     */

    /**
     * Contains event handling methods.
     *
     * @class App.Ext.Core.events
     * @extends App.Base.Core.events
     */

    /**
     * Contains template parsing modules.
     *
     * @class App.Ext.Core.template
     * @extends App.Base.Core.template
     */

    /**
     * Ajax related functions.
     *
     * @class App.Ext.Core.ajax
     * @extends App.Base.Core.ajax
     */


    /**
     * Cookies parser.
     *
     * @class App.Ext.Core.cookie
     * @extends App.Base.Core.cookie
     */


    /**
     * Related to cookie `userdata` functions.
     *
     * @class App.Ext.Core.cookie.userdata
     */
	core.cookie.userdata = (function() {

        /**
         * Cookie "userdata" contains delimited with semicolon user data
         * @private
         */
		function getUserData() {
			var userdata = core.cookie.getCookie("userdata");
			return userdata ? userdata.split(";") : [];
		}

        /**
         * Index 0: "hash" - encrypted data
         *
         * @return {String}
         */
		function getHash() {
			var userdata = getUserData();
			return userdata.length > 0 ? userdata[0] : "";
		}

        /**
         * Index 1: "person name" - plain data
         *
         * @return {String}
         */
		function getPersonName() {
			var userdata = getUserData();
			return userdata.length > 1 ? userdata[1] : "";
		}

		return {
			getHash			: getHash,
			getPersonName	: getPersonName
		}
	}());




    /**
     * Contains the concrete data storage class.
     *
     * @class App.Ext.Core.Store
     */
    core.Store = Store;


    /**
     * Contains the concrete MVC framework.
     *
     * @class App.Ext.Core.mvc
     */
    core.mvc = Backbone;



    "App.Ext".toNamespace() && (App.Ext.Core = core);

	return core;

});
requirejs([
    "widgets/logininfo/models/logininfo"
],
function(LoginInfoModel) {

    describe("LoginInfo model", function() {

        describe("after initialization", function() {

            it("has property 'personName'", function() {

                var model = new LoginInfoModel();

                expect(model.attributes.hasOwnProperty("personName")).toBeTrue();

            });

            it("property 'personName' is a string", function() {

                var model = new LoginInfoModel();

                expect(model.get("personName")).toBeString();

            });

        });

    });

});

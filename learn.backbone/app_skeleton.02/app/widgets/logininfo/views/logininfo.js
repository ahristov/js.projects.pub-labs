define([
	"sandbox",
	"../models/logininfo",
	"text!../tmpl/logininfo.html"
],
function(sandbox, LoginInfoModel, loginInfoTemplate) {

	var loginInfoView = sandbox.mvc.View({

		// tagName: "span",

		template: sandbox.template.parse(loginInfoTemplate),

		// The DOM events specific to an item
		events: {
			"click .logininfo-personname" : "personNameClicked"
		},

		initialize: function() {
			this.model = new LoginInfoModel;
			this.model.bind("change", this.render, this);
		},

		render: function() {
			this.el.innerHTML = this.template(this.model.toJSON());
		},

		personNameClicked: function() {
			alert("login info cicked. Person Name: " + this.model.get("personName"));
		}
	});

	return loginInfoView;
});

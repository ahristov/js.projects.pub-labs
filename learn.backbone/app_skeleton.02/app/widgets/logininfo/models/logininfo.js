define([
	"sandbox"
],
function(sandbox) {

	var LoginInfoModel = sandbox.mvc.Model({

		// Default attributes for the todo.
		defaults: {
			personName: ""
		},

		// Ensure has at least default values.
		// Q: Can't we achieve this with validation???
		//
		initialize: function() {
			if (! this.get("personName")) {
				this.set({
					"personName":
					sandbox.cookie.userdata.getPersonName() || this.defaults.personName
				});
			}
		}


	});

 	return LoginInfoModel;
});

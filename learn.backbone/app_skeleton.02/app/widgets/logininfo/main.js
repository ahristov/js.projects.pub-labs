define([
	"sandbox",
	"./views/logininfo"
],
function(sandbox, LoginInfoView) {
	return sandbox.subscribe("bootstrap", "logininfo", function(element) {
		new LoginInfoView({el: sandbox.dom.find(element)}).render();
	});
});

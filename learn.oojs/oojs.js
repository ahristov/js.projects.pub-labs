
// 1) A counter using object literal
//
var counter = {
	n : 0,
	increment: function() {
		return ++this.n;
	}
};
counter.increment(); // 1
counter.increment(); // 2


// 2) A counter using Object.create 
// and/or Object.defineProperty
//
var counter = Object.create(null, {
	"n": {
		value: 0,
		writable: true,	// is not readonly
		enumerable: true,	// forEach
		configurable: false	// type can not change, can not be deleted
	},
});

Object.defineProperty(counter, "increment", {
	get: function() {
		return ++this.n;
	},
});

counter.increment; // 1
counter.increment; // 2


var o2 = Object.create(counter);


// 3) A counter constructor function
// [and evtl. prototype]
//
function Counter() {
	this.n = 0;
}
Counter.prototype.increment = function() {
	return ++this.n;
};

var counter = new Counter();
counter.increment(); // 1
counter.increment(); // 2


// 4) A counter using module pattern
//
var counter = (function() {
	var n = 0;
	return {
		increment: function() { return ++n; }
	};
})();

counter.increment(); // 1
counter.increment(); // 2


// 5) A counter using revealing module pattern
//

function counter(initial) {
	var n = initial;
	return {
		increment: function() {
			return ++n;
		}
	};
};

var c = counter(10);
c.increment(); // 11
c.increment(); // 12




// A counter using singleton
//
var Counter = (function() {
	
	var instance;
	
	function init() {
		var n = 0;
		return {
			increment: function() {
				return ++n;
			}
		};
	}
	
	return { 
		getInstance: function() {
			if (! instance) {
				instance = init();
			}
			
			return instance;
		}
	}
	
})();

var counter1 = Counter.getInstance();
var counter2 = Counter.getInstance();
counter1.increment(); // 1
counter1.increment(); // 2
counter2.increment(); // 3
counter2.increment(); // 4


// Inheritance and override: Classical default pattern
// Does not allow passing parameters
//

function inherit(C, P) {
	C.prototype = new P();
}

function Counter() {
	this.n = 0;
}
Counter.prototype.increment = function() {
	return ++this.n;
};

function DblCounter() {};
inherit(DblCounter, Counter);

DblCounter.prototype.increment = function() {
	this.n += 2;
	return this.n;
};


var counter = new Counter();
counter.increment(); // 1
counter.increment(); // 2

var dblCounter = new DblCounter();
dblCounter.increment(); // 2
dblCounter.increment(); // 4


// Inheritance and override: Rent-a-constructor
// Allows passing parameters
//

function Counter(initial) {
	this.n = initial;
}
// The next can not be inherited using this pattern
Counter.prototype.increment = function() {
	return ++this.n;
};
function DblCounter(initial) {
	Counter.apply(this, arguments);
};
// This is just defined for first time, now overriden.
DblCounter.prototype.increment = function() {
	this.n += 2;
	return this.n;
};

var counter = new Counter(100);
counter.increment(); // 1
counter.increment(); // 2

var dblCounter = new DblCounter(100);
dblCounter.increment(); // 102
dblCounter.increment(); // 104


// Inheritance and override: Rent-a-constructor
// Allows multiple inheritance
//
function Cat() {
	this.legs = 4;
	// Can inherit only if in the ctor function.
	// The next will be inherited.
	this.say = function() {
		return "meaowww";
	}
}
// The next will not be inherited!!!!!!
// Cat.prototype.say = function() {
//	return "meaowww";
// };

function Bird() {
	this.wings = 2;
	this.canFly = true;
}

function CatWithWings() {
	Cat.apply(this, arguments);
	Bird.apply(this, arguments);
}

var pet = new CatWithWings();
console.dir(pet);



// Inheritance using extend().
// Allow multiple inheritance.
// Gets copy from base.prototype

Object.prototype.extend = function(extension) {
	for (var key in extension) {
		if (! this[key]) {
			this[key] = extension[key];
		}
	}
	return this;
};

function Cat(name) {
	this.name = name;
	this.legs = 4;
}
Cat.prototype.isSaying = function() {
	return "meaowww";
};

function Bird() {
	this.wings = 2;
}
Bird.prototype.isFlying = function() {
	return true;
};

function FlyingCat(name) {
	this.extend(new Cat(name));
	this.extend(new Bird());
}

var flyingCat = new FlyingCat("Oscar");
flyingCat.isSaying();
flyingCat.isFlying();

var arr = [].extend(new Cat());


// Inheritance and override: Rent-and-set prototype
// Does not allow multiple inheritance.
//

function Counter() {
	this.n = 0;
}
Counter.prototype.increment = function() {
	return ++this.n;
};

function CounterSet(initial) {
	Counter.apply(this, arguments);
	this.n = initial;
};
CounterSet.prototype = new Counter();

var counterSet = new CounterSet(100);
counterSet.increment(); // 101
counterSet.increment(); // 102




// Hoisting
// All variables, no matter where in the function body they are declared, get hoisted
// to the top of the function.
// When using function declaration, also the definition of the function gets hoisted.
//
function hoistMe() {

	console.log(typeof foo); // function
	console.log(typeof bar); // undefined
	
	foo(); // local foo
	bar(); // TypeError: bar is not a function
	
	// Both variable foo and it's implementation gets hoisted
	function foo() { 
		console.log("local foo"); 
	}
	
	// Only variable gets hoisted!!
	var bar = function() {
		console.log("local bar");
	};

}

hoistMe();


// Execution context
//
// 1. Global
// 2. Function
// 3. Eval code
//
// Every time we call a function, an execution context will be added to execution stack:
// 
// > Current execution context
// > Execution context 2
// > Execution context 1
// > Global execution context
//
// all variables in the stack below the current execution context 
// are visible in the current execution context.
//
//
// Two steps when creating execution context:
//
// 1) Creation Stage
// 1.1) Create arguments, functions, variables (in that exactly order)
// 1.2) Create the scope chain
// 1.3) Determine the value of "this"
//
// 2) Execute Stage
//

(function() {

	// In this example:
	// ----------------
	// functions are created before variables
	// thereof at activation stage,:
	// 1) declares function variables, therreof:
	// 1.1) declares variable foo
	// 1.2) initializes foo (functions will always be initialized).
	// 2) declares variables, thereof:
	// 2.1) it sees "foo" was declared
	// 2.2) does not redeclare it
	// and does not redeclare the variable "foo" from above
	//

    console.log(typeof foo); // function pointer
    console.log(typeof bar); // undefined

    var foo = 'hello',
        bar = function() {
            return 'world';
        };

	
    function foo() {		
        return 'hello';
    }

	console.log(typeof foo); // string
	console.log(typeof bar); // function
}());


(function() {

    function foo() {		
        return 'hello';
    }

	function inner() {
		console.log(typeof foo); // undefined
		console.log(typeof bar); // undefined

		var foo = 'hello',
			bar = function() {
				return 'world';
			};

		console.log(typeof foo); // string
		console.log(typeof bar); // function
	}

	inner();
	
}());






/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 5/10/12
 * Time: 11:34 AM
 *
 * HTML5Video experiments.
 */


var Site = Site || {};


///////////////////////////////////////////////////////////////////////////////

Site.Utils = {

	/**
	 * Gets the data about the key for keyboard evt.
	 *
	 * @param {Object} evt The keyboard event object
	 * @return {Object} An object with properties 'keyCode', 'char', 'keyName'
	 */
	getKey: function(evt) {

		var keyCode = 0;
		var char    = "";
		var keyName = "";

		/**
		 * Specific to Opera on the STB
		 * @param {Object} evt The keyboard event object
		 * @return {String} the name of the key.
		 */
		function getKeyName(evt) {

			var keyName = "";

			console.log("getKeyName - Received evt.keyCode="+evt.keyCode+",evt.ctrlKey="+evt.ctrlKey+",evt.shiftKey="+evt.shiftKey+",evt.altKey="+evt.altKey+"");

			switch (evt.keyCode) {
				case 917528:
					if (event.ctrlKey == true &&
						event.shiftKey ==false &&
						event.altKey == false ) {
						keyName = "Play";
						break;
					}
					break;
				case 917507:
					if (event.ctrlKey == false &&
						event.shiftKey == false &&
						event.altKey == true ) {
						keyName = "Pause";
						break;
					}
					break;
				case 917523:
					if (event.ctrlKey == true &&
						event.shiftKey == false &&
						event.altKey == false ) {
						keyName = "Rewind";
						break;
					}
					break;
				case 917524:
					if (event.ctrlKey == true &&
						event.shiftKey == false &&
						event.altKey == false ) {
						keyName = "Forward";
						break;
					}
					break;
			}

			console.log("getKeyName - returns "+keyName+"");

			return keyName;
		}


		//some browsers support evt.charCode, some only evt.keyCode
		var keyCode = evt.charCode
			? evt.charCode
			: evt.keyCode;


		//all real characters
		if (keyCode > 31 && keyCode != 127 && keyCode < 65535) {
			// get the character the user entered.
			char = String.fromCharCode(keyCode);
		}

		keyName = getKeyName(evt);

		return {
			keyCode : keyCode,
			char    : char,
			keyName : keyName
		};
	}

};


///////////////////////////////////////////////////////////////////////////////


Site.Html5Video = function(videoEl) {

	var self = this;

	self.spyEvents = function(handler) {

		var events = [
			'abort', 'canplay', 'canplaythrough', 'durationchange',
			'emptied', 'empty', 'ended', 'error', 'loadeddata',
			'loadedmetadata', 'loadstart', 'pause', 'play', 'playing',
			'progress', 'ratechange', 'seeked', 'seeking', 'stalled',
			'suspend', 'timeupdate', 'volumechange', 'waiting'
		];

		function mediaEventHandler(e) {
			if (!e) { e = window.event; }
			handler(e);
		}

		for (var i in events) {
			videoEl.addEventListener(events[i], mediaEventHandler, false);
		}
	};

	self.spyProps = function(handler) {

		var props = [
			'autoplay',	'buffered',	'controls',	'currentSrc',
			'currentTime', 'defaultPlaybackRate', 'duration',
			'ended', 'error', 'height', 'loop', 'muted',
			'networkState', 'paused', 'playbackRate',
			'played', 'poster', 'preload', 'readyState',
			'seekable', 'seeking', 'videoHeight', 'videoWidth',
			'volume', 'width'
		];

		for (var i in props) {
			handler(props[i], videoEl[props[i]]);
		}

		setTimeout(function() { self.spyProps(handler) }, 1300);

	}

};

///////////////////////////////////////////////////////////////////////////////

Site.Page = (function(videoElID) {

	var videoEl 	= document.getElementById(videoElID);
	var oHtmlVideo	= new Site.Html5Video(videoEl);

	/**
	 * Handlers for keyboard events.
	 *
	 * @param {Object} evt The keyboard event object
	 */
	function onKeyPress(evt) {

		var keyData = Site.Utils.getKey(evt);

		if (keyData.keyName=="Play" || keyData.char=="p") {
			videoEl.playbackRate = 1;
			videoEl.play();
		}

		if (keyData.keyName=="Pause" || keyData.char=="s") {
			videoEl.pause();
		}

		if (keyData.keyName=="Rewind" || keyData.char=="r") {
			console.log('start rewind...');
			videoEl.playbackRate = -1.0;
			videoEl.play();
		}

		if (keyData.keyName=="Forward" || keyData.char=="f") {
			videoEl.playbackRate = +2.0;
			videoEl.play();
		}

	}

	function showVideoEvent(e) {

		var msg,
			el = e.srcElement,
			time = Math.round(el.currentTime*100)/100,
			duration = Math.round(el.duration*100)/100,
			playRate = el.playbackRate;

		switch (e.type) {
			case "play":
			case "waiting":
			case "progress":
				msg = el.currentSrc;
				break;
			default:
				msg = time+"s/"+duration+"s, speed:"+playRate;
		}

		console.log("HTMLMediaElement.Event->"+e.type+":"+msg);
	}

	function showVideoProp(name, prop) {
		console.log(name);
		console.dir(prop);
	}

	function onLoad() {

		console.log('loaded document.');

		window.onkeypress = onKeyPress;

		oHtmlVideo.spyEvents(showVideoEvent);

		// oHtmlVideo.spyProps(showVideoProp);

	}

	function onUnLoad() {

		console.log('unloaded document.');

	}



	
	return {
		onLoad		: onLoad,
		onUnLoad	: onUnLoad
	};

}('video'));


///////////////////////////////////////////////////////////////////////////////


// End: site.js

@ECHO OFF

REM ##########################################################################
REM Make the manual documentation. Parse Markdown files into html files.
REM ##########################################################################

"C:\python27\python.exe" "C:\python27\Scripts\markdown" doc\README.mdown > doc\index.html
"C:\python27\python.exe" "C:\python27\Scripts\markdown" doc\TODO.mdown > doc\todo.html

exit 0


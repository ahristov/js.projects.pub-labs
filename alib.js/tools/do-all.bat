@ECHO OFF

REM ##########################################################################
REM Run complete build
REM For the Hudson run the steps separately though 
REM ##########################################################################

call tools\make-mdown.bat
call tools\make-apidoc.bat
call tools\run-tests.bat
call tools\do-compile.bat

exit 0

@ECHO OFF

REM ##########################################################################
REM Runs compiler/minifier
REM ##########################################################################

REM Combine in one uncompressed file, keep comments
type ^
	src\a_root.js					^
	src\a_utils.js					^
  > lib\a.dev.js

REM Compile without optimization
java -jar ..\tools\compiler.jar ^
  --formatting PRETTY_PRINT ^
  --compilation_level SIMPLE_OPTIMIZATIONS ^
  --js lib\a.dev.js ^
  --js_output_file lib\a.src.js

REM Compile with optimization
java -jar ..\tools\compiler.jar ^
  --compilation_level SIMPLE_OPTIMIZATIONS ^
  --js lib\a.dev.js ^
  --js_output_file lib\a.min.js
  

exit 0

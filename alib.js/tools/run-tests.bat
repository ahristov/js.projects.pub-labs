@ECHO OFF

REM ##########################################################################
REM Runs unit tests
REM From Hudson specify as test results "TEST-*.xml" go grap the output
REM from all the test fixtires for all the browsers attached.
REM ##########################################################################

java -jar ..\tools\JsTestDriver-1.3.2.jar --tests all --testOutput . && exit %ERRORLEVEL%


@ECHO OFF

REM ##########################################################################
REM Build the project's API documentation
REM ##########################################################################

rmdir /S /Q doc\generator
..\tools\yuidoc\bin\yuidoc.py src -p doc\parser -o doc\generator -t doc\template -Y 3 -m "ALib Library" && exit %ERRORLEVEL%



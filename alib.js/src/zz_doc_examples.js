/**
 * A javascript library
 * Examples of YUI documentation formating
 * @module zz_doc_examples
 * @author Atanas Hristov
 */ 

// param types:
// Object, Array, String, Boolean, Number, Mixed, MyType
// 
 
// When the code gets split into different directories
// first is to define the top object exists already.
var ALib = ALib || {};

ALib.namespace('ALib.doc.examples');

/**
 * Documentation examples object
 * @namespace ALib.zz_doc.examples
 * @class zz_doc_examples 
 */
ALib.doc.examples = {
	/**
	 * Keep reference to the global object (aka this).
	 * @method global
	 * @static
	 * @return {Reference} A reference to the global object (aka this).
	 */
	global: (function() {
		return this; 
	}())
	
};


/**
 * Example class with constructor function.
 * @namespace ALib.zz_doc.examples
 * @class Person
 * @constructor
 * @param {String} first_name First name
 * @param {String} last_name Last name
 */
ALib.doc.examples.Person = function (first_name, last_name) {
	
	/**
	 * First name of the person
	 * @property first_name
	 * @type String
	 */
	 this.first_name = first_name;
	 
	/**
	 * Last (family) name of the person
	 * @property last_name
	 * @type String
	 */
	 this.last_name = last_name;
	 
};

/**
 * Returns the name of the person object.
 * @method getName
 * @return {String} The name of the person.
 */
ALib.doc.examples.Person.prototype = function() {
	return this.first_name + ' ' + this.last_name;
};




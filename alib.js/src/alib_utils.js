/**
 * A javascript library
 * @module alib_utils
 * @author Atanas Hristov
 */ 

var ALib = ALib || {};

ALib.namespace('Utils');


/**
 * Array class.
 * Usage example:
 *	ALib.Sandbox('*', function(box) {
 *		var arr = new box.Utils.Array(["1","2","3","4"]);
 *		console.log(arr.elements);
 *		console.log(arr.indexOf("2"));
 *	});
 *
 * @namespace ALib.utils
 * @class Array
 * @constructor
 */
ALib.Utils.Array = (function() {

	var astr = "[object Array]",
		toString = Object.prototype.toString,
		Constr;

	// private methods -- implementation
	
	/**
	 * Provides casting of an object to Array
	 * @method toArray
	 * @param {Object} obj Object to cast to Array
	 * @return {Array} Array object
	 */
	function toArray(obj) {
		for (var i = 0, a=[], len = obj.length; i < len; i += 1) {
			a[i] = obj[i];
		}
		return a;
	}
	
	/**
	 * Check if the provided parameter
	 * is of type Array
	 * @method isArray
	 * @param {Object} a Object to check if it as instance of the Array class
	 * @return {Boolean} Is the provided parameter 'a' an instance of the Array class
	 */
	function isArray(a) {
		return toString.call(a) === astr;
	}

	/**
	 * Get the index of the first occurance of 'searchFor' into the array's elements
	 * or -1 if not found
	 * @method indexOf
	 * @param {Object} searchFor element to search for
	 * @return {Integer} The first index where the element 'searchFor' appears 
	 *   into the array's elements or -1 if not found.
	 */
	function inArray(searchFor) {
		for (var i = 0, len = this.elements.length; i < len; i += 1) {
			if (this.elements[i] === searchFor) {
				return i;
			}
		}
		return -1;
	}
	
	
	// public API -- constructor
	Constr = function(o) {
		this.elements = toArray(o);
	};
	
	// public API -- prototype
	Constr.prototype = {
		constructor: ALib.Utils.Array,
		verson: "0.1",
		isArray: isArray,
		toArray: toArray,
		indexOf: inArray
	};

	// Return the constructor to be assigned
	// to the namespace.
	return Constr;
}());
 
/**
 * A javascript library
 * @module alib
 * @class ALib
 */ 


// When the code gets split into different directories
// first is to define the top object exists already.
var ALib = ALib || {};

 /**
 * Implements namespacing.
 *
 * <pre>
 * Usage example:
 *
 * ALib.namespace('ALib.UI.Windows') 
 * -or-
 * ALib.namespace('ALib.UI.Windows')
 *
 * </pre>
 * 
 * @method namespace
 * @param {String} ns_string Namespace in the form of 'Lib.Path.Path'.
 */
ALib.namespace = function (ns_string) {
	var parts = ns_string.split('.'),
		parent = ALib,
		i,
		len;
	
	// strip redundant leading global
	if (parts[0] === 'ALib') {
		parts = parts.slice(1);
	}
	
	for (i = 0, len=parts.length; i < len; i += 1) {
		// create a property if it does not exist
		if (typeof parent[parts[i]] === "undefined") {
			parent[parts[i]] = {};
		}
		parent = parent[parts[i]];
	}
	
	return parent;
};

/**
 * Implements mixin pattern.
 *
 * <pre>
 * Usage example:
 *
 * </pre>
 * 
 * @method mixin
 * @param {Object} obj1 Object to borrow properties from.
 * @param {Object} obj2 Object to borrow properties from.
 * @param {Object} objN Object to borrow properties from.
 */
ALib.mixin = function() {
	var i, prop, child = {};
	for (i = 0; i < arguments.length; i += 1) {
		for (prop in arguments[i]) {
			if (arguments[i].hasOwnProperty(prop)) {
				child[prop] = arguments[i][prop];
			}
		}
	}
	return child;
}

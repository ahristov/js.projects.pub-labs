

TestCase("ALib.Test", {

	"test - namespace: Should create deep structure": function() {
	
		ALib.namespace('Level1.Level2.Level3');
		
		assertNotUndefined(ALib);
		assertNotUndefined(ALib.Level1);
		assertNotUndefined(ALib.Level1.Level2);
		assertNotUndefined(ALib.Level1.Level2.Level3);
		
	},

	"test - mixin: Should borrow from multiple": function() {

		function Parent(fName, lName) {
			function getFullName() {
				return fName + ' ' + lName;
			}
			
			return {
				firstName : fName,
				lastName : lName,
				getFullName : getFullName,
				sayWhat : function() { return 'I like cars!'; }
			}
		}
		
		function Child(fName, lName) {
			
			var child = ALib.mixin(Parent(fName, lName));
			
			child.sayWhat = function() { return 'I like toys!'; }
			
			return child;
		}

		var parent = Parent('Atanas', 'Hristov');
		parent.parent = Parent('Yordan', 'Hristov');
		
		var child = Child('Alex', 'Hristov');
		
		assertEquals('Atanas', parent.firstName);
		assertEquals('Hristov', parent.lastName);
		assertEquals('Atanas Hristov', parent.getFullName());
		
		assertEquals('Alex', child.firstName);
		assertEquals('Hristov', child.lastName);
		assertEquals('Alex Hristov', child.getFullName());
		
		assertEquals('I like cars!', parent.sayWhat());
		assertEquals('I like toys!', child.sayWhat());
		
		
	}

	
});
###
  # COMMENTS
###



# This is an one-line comment

###
This is a multiline comment
###


###
  # VARIABLES
###

# This is a local variable
myVariable = "test"

# This is a global variable
exports = this;
exports.MyVariable = "foo-bar"


###
  # FUNCTIONS
###

func1 = -> "bar"

func2 = ->
  # An extra line
  "bar"

times1 = (a, b) -> a * b

times2 = (a = 1, b = 2) -> a * b

sum = (nums...) ->
  result = 0
  nums.forEach (n) -> result += n
  result



###
  # FUNCTION INVOCATION
###

a = "Howdy!"

alert a
# same as
alert(a)


alert inspect a
# Equivalent to:
alert(inspect(a))



/*
  # COMMENTS
*/

/*
This is a multiline comment
*/

/*
  # VARIABLES
*/

(function() {
  var a, exports, func1, func2, myVariable, sum, times1, times2,
    __slice = Array.prototype.slice;

  myVariable = "test";

  exports = this;

  exports.MyVariable = "foo-bar";

  /*
    # FUNCTIONS
  */

  func1 = function() {
    return "bar";
  };

  func2 = function() {
    return "bar";
  };

  times1 = function(a, b) {
    return a * b;
  };

  times2 = function(a, b) {
    if (a == null) a = 1;
    if (b == null) b = 2;
    return a * b;
  };

  sum = function() {
    var nums, result;
    nums = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    result = 0;
    nums.forEach(function(n) {
      return result += n;
    });
    return result;
  };

  /*
    # FUNCTION INVOCATION
  */

  a = "Howdy!";

  alert(a);

  alert(a);

  alert(inspect(a));

  alert(inspect(a));

}).call(this);
